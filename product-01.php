<?php $page = "our_product"; ?>

<?php include('inc_header.php'); ?>

<!-- middle -->

<section>

    <div class="wrapper">

        <div id="banner-content"><img src="images/slider/banner-product.jpg" alt="Our products"></div>

        <nav class="share">

            <div class="left">Share:

                <a href="#"><img src="images/material/nav-tw.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-fb.png" alt=""></a>

                <a href="#"><img src="images/material/nav-mail.png" alt=""></a>

            </div>

            <div class="right">

                <a href="#"><img src="images/material/nav-zoomin.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-zoomout.png" alt=""></a>

                <a href="#"><img src="images/material/nav-print.png" alt=""></a>

            </div>

        </nav>

        <aside>

            <ul class="submenu">

                <li><a href="product-01.php" class="active">Group Life</a></li>

                <li><a href="product-02.php">Group Health</a></li>

                <li><a href="product-03.php">Group Saving</a></li>
                
            </ul>

            <div class="side_link ">

                <div class="label">sequis link</div>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid1.png" alt="my sequis" /></span>

                    <span class="text">

                        <h6>my Sequis</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid2.png" alt="Sequisfriend"/></span>

                    <span class="text">

                        <h6>Sequisfriend</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid3.png" alt="Calculator"/> </span>

                    <span class="text">

                        <h6>Calculator</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

            </div>

            <address>

                <div class="label">get in touch</div>

                <div>

                    <img src="images/material/icon-pointer.png" alt="">

                    <h6>Sequis Group</h6>

                    <p>Sequis Center Lt. 5<br />

                        Jl. Jend. Sudirman No. 71<br />

                        Jakarta 12190, Indonesia<br />

                        T. +6221 5226 677<br />

                        F. +6221 5205 837

                    </p>

                </div>

                <a href="#">Get Direction</a> <a href="#">Send Message</a>

            </address>

        </aside>

        <div id="content">

            <nav class="breadcumb"><a href="#">Home</a> / <a href="#">Our Products </a> / <a href="#">Group Life </a></nav>

            <h2>Group Life</h2>

            <div class="img_content">

                <img src="images/content/img-about.jpg" alt="">

            </div>

            <h6>Group Term Life</h6>
            <p>
  Merupakan asuransi jiwa berjangka kolektif yang diperuntukkan bagi karyawan aktif perusahaan yang terdaftar di Indonesia dengan periode pertanggungan 1 tahun dan dapat diperpanjang. Manfaat tambahan yang bisa didapatkan dari perlindungan ini antara lain kecelakaan, cacat tetap, penyakit kritis, dan rawat inap di RS,fasilitas free cover limit untuk jumlah pertanggungan tertentu, bebas pajak bagi program kesejahteraan.</p>
<ul class="ul_style">
  <li> <strong>Total Permanent Disability (TPD)</strong><br />
    Asuransi tambahan yang memberikan manfaat pertanggungan jika peserta mengalami cacat total <br />
  permanen yang diakibatkan penyakit atau kecelakaan.</li>
  <li> <strong>Accidental Death and Disability Benefit (ADDB)</strong><br />
    Asuransi tambahan yang memberikan manfaat pertanggungan jika peserta meninggal dunia (100% UP) atau menderita cacat tetap akibat kecelakaan (% UP sesuai tabel manfaat). Manfaat tambahan yang bisa didapatkan dari produk asuransi ini antara lain santunan meninggal dunia dan cacat tetap akibat kecelakaan, penggantian biaya pengobatan akibat kecelakaan, penggantian biaya pemakaman akibat kecelakaan.</li>
  <li> <strong>Critical Illness</strong><br />
    Asuransi tambahan yang memberikan manfaat sebesar uang pertanggungan Critical Illnes jika peserta di-diagnosa menderita salah satu dari 36 penyakit kritis selama periode polis.
  </li>
</ul>


        </div>

        <div class="clear"></div>

    </div>

</section>

<!-- end of middle -->

<?php include('inc_footer.php'); ?>