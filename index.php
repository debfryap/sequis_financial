<?php
$page = "";
include('inc_header.php');
?>
<!-- middle -->
<section>
    <div class="wrapper">
        <div id="widget" class="side_link">
            <div class="items">
                <div class="label">Our Product</div>
                <a href="product-01.php">
                    <span class="ico"><img src="images/material/sq_link_1.png" alt="my sequis" /></span>
                    <span class="text">
                        <h6>Group Life</h6>
                        <p>This is Photoshop's version  of Lorem Ipsum. </p>
                    </span>
                </a>
                <a href="product-02.php">
                    <span class="ico"><img src="images/material/sq_link_2.png" alt="Sequisfriend"/></span>
                    <span class="text">
                        <h6>Group Saving</h6>
                        <p>This is Photoshop's version  of Lorem Ipsum. </p>
                    </span>
                </a>
                <a href="product-03.php">
                    <span class="ico"><img src="images/material/sq_link_3.png" alt="Calculator"/> </span>
                    <span class="text">
                        <h6>Calculator</h6>
                        <p>This is Photoshop's version  of Lorem Ipsum. </p>
                    </span>
                </a>
                <a href="" class="loc_link">View Our Offices</a>

            </div>
        </div>
        <div class="slider-wrapper theme-default afterclear">            
            <div id="slider" class="nivoSlider"> 
                <img src="images/slider/banner-01.jpg" alt="" title="#htmlcaption"/> 
                <img src="images/slider/banner-02.jpg" alt="" title="#htmlcaption"/> 
                <img src="images/slider/banner-01.jpg" alt="" title="#htmlcaption"/> 
                <img src="images/slider/banner-02.jpg" alt="" title="#htmlcaption"/> 
            </div>
            <div id="htmlcaption" class="nivo-html-caption">
                <h4>Sequis Q</h4>
                <h1>smartlife</h1>
                <p>Sequis Q Smart Life is a pure life insurance product that gives you life protection and assurance to you and your family up until aged 100 years. </p>
                <a href="#">Learn More</a> 
            </div>
            <div class="triangel"></div>
        </div>

        <div id="widget">
            <div class="items about">
                <div class="label">about us</div>
                <h4>We stand behind our commitment to serve our customer for a lifetime.</h4>
                <p><a href="about_us.php">Continue Reading</a></p>
            </div>
            <div class="spar"></div>
            <div class="items customers">
                <div class="box_customers afterclear">
                    <div class="label">Our Customers</div>
                    <a href="process_claim_1.php">
                        <img src="images/material/customer_linkico.png" alt="" />
                        <span>process claim</span>
                    </a>
                    <a href="payment_method_1.php">
                        <img src="images/material/customer_linkico_2.png" alt="" />
                        <span>payment methods</span>
                    </a>
                    <a href="panduan_layanan_1.php">
                        <img src="images/material/customer_linkico_3.png" alt="" />
                        <span>panduan layanan</span>
                    </a>
                    <!--<a href="hospital_list.php">
                        <img src="images/material/customer_linkico_4.png" alt="" />
                        <span>hospital list</span>
                    </a> -->                   
                </div>
                <a href="" class="financial_download">
                    Financial Report 2013
                </a>
                <!--<a href="" class="sqf">
                    SQF Portal
                </a>-->
            </div>

            <div class="spar"></div>
            <div class="items unit-link">
                <div class="row">
                    <div class="box-grey"><span class="title">Unit link price</span><span class="date">18</span> <span class="small">sep 2014</span></div>
                    <h2><span>Rupiah Equity Fund</span>1.393,31</h2>
                </div>
                <div class="row">
                    <div class="box-grey"><span class="title">exchange rate</span><span class="date">18</span> <span class="small">sep 2014</span></div>
                    <h2><span>&nbsp</span>11.987,00</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="js/jquery.nivo.slider.js"></script> 
<script type="text/javascript">
    $(window).load(function () {
        $('#slider').nivoSlider({
            directionNav: false,
        });
    });
</script> 
<!-- end of middle -->
<?php include('inc_footer.php'); ?>