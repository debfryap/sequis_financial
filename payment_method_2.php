<?php $page = "our_customers"; ?>

<?php include('inc_header.php'); ?>

<!-- middle -->

<section>

    <div class="wrapper">

        <div id="banner-content"><img src="images/slider/banner-ourcust.jpg" alt="Our Customers"></div>

        <nav class="share">

            <div class="left">Share:

                <a href="#"><img src="images/material/nav-tw.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-fb.png" alt=""></a>

                <a href="#"><img src="images/material/nav-mail.png" alt=""></a>

            </div>

            <div class="right">

                <a href="#"><img src="images/material/nav-zoomin.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-zoomout.png" alt=""></a>

                <a href="#"><img src="images/material/nav-print.png" alt=""></a>

            </div>

        </nav>

        <aside>

            <ul class="submenu">
                <li><a href="#" class="parent">Process Claim</a>
                	<ul>
                        <li><a href="process_claim_1.php">Pengajuan Proses Klaim </a></li>
                        <li><a href="process_claim_2.php">Pengajuan Pembayaran Klaim Meninggal Dunia </a></li>
                        <li><a href="process_claim_3.php">Pengajuan Klaim (selain klaim meninggal dunia) </a></li>
                        
                    </ul>
                </li>
                <li class="expand">  
                    <a href="#" class="parent">Payment Methods</a>
                    <ul>
                        <li><a href="payment_method_1.php">Bank Transfer</a></li>
                        <li><a href="payment_method_6.php">ATM</a></li>
                        <li><a href="payment_method_2.php" class="active">Internet Banking</a></li>
                        <li><a href="payment_method_3.php">Mobile Banking</a></li>
                        <!--<li><a href="payment_method_4.php">SMS Banking</a></li>-->
                        <li><a href="payment_method_5.php">Auto Debet</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="parent">Panduan Layanan</a>
                    <ul>
                        <li><a href="panduan_layanan_1.php">Pembatalan Pembayaran Premi Melalui Pendebetan Rekening/Kartu Kredit</a></li>
                        <li><a href="panduan_layanan_2.php">Penarikan Nilai Tunai/Dana Investasi</a></li>
                        <li><a href="panduan_layanan_3.php">Pemulihan Polis</a></li>
                        <li><a href="panduan_layanan_4.php">Pengajuan Transaksi Unit Link</a></li>
                        <li><a href="panduan_layanan_5.php">Pengajuan Pinjaman Polis</a></li>
                        <li><a href="panduan_layanan_6.php">Pengajuan Perubahan Polis</a></li>
                        <li><a href="panduan_layanan_7.php">Pengajuan Duplikat Polis dan Kartu Kesehatan</a></li>
                        <li><a href="panduan_layanan_8.php">Penarikan Manfaat Tunai dan Deviden</a></li>
                        <li><a href="panduan_layanan_9.php">Pengajuan Bankers Clause / Klausula Ban</a></li>
                    </ul>
                </li>
                <li><a href="hospital_list.php">Hospital List</a></li>
            </ul>

            <div class="side_link ">

                <div class="label">sequis link</div>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid1.png" alt="my sequis" /></span>

                    <span class="text">

                        <h6>my Sequis</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid2.png" alt="Sequisfriend"/></span>

                    <span class="text">

                        <h6>Sequisfriend</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid3.png" alt="Calculator"/> </span>

                    <span class="text">

                        <h6>Calculator</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

            </div>

            <address>

                <div class="label">get in touch</div>

                <div>

                    <img src="images/material/icon-pointer.png" alt="">

                    <h6>Sequis Group</h6>

                    <p>Sequis Center Lt. 5<br />

                        Jl. Jend. Sudirman No. 71<br />

                        Jakarta 12190, Indonesia<br />

                        T. +6221 5226 677<br />

                        F. +6221 5205 837

                    </p>

                </div>

                <a href="#">Get Direction</a> <a href="#">Send Message</a>

            </address>

        </aside>

        <div id="content">

            <nav class="breadcumb"><a href="#">Home</a> / <a href="#">Our Customers </a> / <a href="#">Payment Methods</a> / <a href="#">Internet Banking</a></nav>

            <h2>Internet Banking</h2>
			
            <div class="accordion">
                <div class="head">
                    Klik BCA
                </div>
                <div class="content">
                    <table>                
                    <tr>
                    	<td><strong>Ketik USER ID Anda.<br>
									Kemudian ketik PIN Internet Banking Anda.<br>
									Klik tombol Login
							</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/klik-bca/1.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Klik opsi Pembayaran pada kolom Menu di kiri layar Anda.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/klik-bca/2.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Klik opsi Asuransi pada kolom Pembayaran di sebelah kiri layar Anda.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/klik-bca/3.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Pilih A.J. Sequis Life untuk kolom Perusahaan Asuransi<br>
                            Ketik Nomor Polis Sequislife Anda<br>
                            Isi Jumlah Pembayaran sesuai dengan premi Sequislife Anda<br>
                            Kemudian klik tombol Lanjutkan
                            </strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/klik-bca/4.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Informasi transaksi akan ditampilkan. Periksa sebelum memasukkan kode KeyBCA Anda.
                            Setelah itu klik tombol Kirim.
                            </strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/klik-bca/5.jpg" width="350"></td>
                    </tr>                                                           
                </table>

                </div>

                <div class="head">
                    Bank CIMB NIAGA
                </div>
                <div class="content">
                  <table>                
                    <tr>
                    	<td><strong>Ketik USER ID Anda.<br>
                                    Kemudian ketik PIN Internet Banking Anda.<br>
                                    Klik tombol Login
							</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-cimb/1.jpg" width="350"></td>
                    </tr>                    
                    <tr>
                    	<td><strong>Lakukan verifikasi untuk Akun CIMB Niaga Internet Banking Anda.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-cimb/2.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Klik opsi PayBills<br>
                                Kemudian klik kolom Payment Type dan pilih opsi Insurance
							</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-cimb/3.jpg" width="350"></td>
                    </tr>
                     <tr>
                    	<td><strong>Pilih Payment Name Sequislife</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-cimb/4.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Ketik Nomor Polis Sequislife Anda<br>
                            		Isi Jumlah Tagihan sesuai dengan premi Sequislife Anda
                            </strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-cimb/5.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Pilih Jenis Pembayaran.<br>
                            		Lalu klik opsi Lanjutkan
                            </strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-cimb/6.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Layar akan meminta nasabah untuk memasukkan mPIN (PIN tambahan untuk transaksi finansial yang akan dikirimkan CIMB Niaga pada telepon genggam nasabah melalui SMS)<br>
                            Masukkan mPIN
                            </strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-cimb/7.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Konfirmasi Transaksi Berhasil akan ditampilkan.<br> 
                                    Klik Cetak bila menginginkan bukti transaksi.
                            </strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-cimb/8.jpg" width="350"></td>
                    </tr>                                                          
                </table>  
                    
                </div>
                
                <div class="head">
                    Bank Mandiri
                </div>
                <div class="content">
                    <table>                
                    <tr>
                    	<td><strong>Ketik USER ID Anda.<br>
                                    Kemudian ketik PIN Internet Banking Anda.<br>
                                    Klik tombol Login
							</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-mandiri/1.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Klik opsi Payment/Pembayaran<br>
                                    Lalu klik opsi Insurance/Asuransi
							</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-mandiri/2.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Pilih opsi 23008 Sequis Life pada kolom Multi Payment<br>
                                    Isi kolom yang tersedia<br>
                                    Klik opsi Lanjutkan/Conntinue untuk meneruskan transaksi.
							</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-mandiri/3.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Muncul nominal yang diinput, beri tanda centang.<br>
                                    Klik opsi Lanjutkan/Conntinue untuk meneruskan transaksi.
							</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-mandiri/4.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Konfirmasi transaksi akan ditampilkan. <br>
                                    Periksa sebelum memasukkan challenge code dari Token.<br>
                                    Setelah itu klik tombol Kirim/Send.
							</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-mandiri/5.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Transaksi selesai,<br>
                                    Sistem akan memunculkan validasi yang dapat disimpan atau dicetak.
							</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-mandiri/6.jpg" width="350"></td>                    </tr>                                                                                                  
                </table>
                    
                </div>
                
                <div class="head">
                    Bank Permata
                </div>
                <div class="content">
                    <table>                
                    <tr>
                    	<td><strong>Ketik USER ID Anda.<br>
                                    Kemudian ketik PASSWORD Internet Banking Anda.<br>
                                    Klik tombol Login
							</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-permata/1.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Pada Transaction Menu, pilih opsi Payment</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-permata/2.png" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Pilih nama merchant lalu pilih opsi Virtual Account</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-permata/3.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Pada layar Bill Information, ketik Nomor Virtual Account (880300) diikuti  10 digit Nomor Polis Sequislife)<br>
		                            Kemudian klik tombol Next
                            </strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-permata/4.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Isi jumlah pembayaran sesuai dengan premi Sequislife Anda di kolom Payment.<br>
                                    Pilih Source Account yang Anda inginkan<br>
                                    Klik tombol Next
							</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-permata/5.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Informasi transaksi akan ditampilkan.<br>
                                    Periksa sebelum menekan tombol Add To Transaction Basket
							</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-permata/6.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Klik tombol Transact Now! pada layar Transaction Basket </strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-permata/7.png" width="350"></td>
                    </tr>                                                                                                                 
                </table>                    
                </div>
                
                
                <div class="head">
                    Bank BNI
                </div>
                <div class="content">
                    <table>                
                    <tr>
                    	<td><strong>Ketik USER ID Anda.<br>
                            Kemudian ketik PIN Internet Banking Anda.<br>
                            Kemudian masukan karakter<br>
                            Klik tombol Login

							</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-bni/1.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Klik opsi Transfer Antar Rek BNI<br>
                                    Masukan No. Rek BNI Sumber Dana<br>
                                    Masukan No. Rek Tujuan
							</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-bni/2.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Input semua kolom mandatori yang bertanda*<br>
                                    Input No VA pada kolom input rekening<br>
                                    (contoh 8803000000000123) – 6 digit company code – 10 digit policy code
                            </strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-bni/3.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Lengkapi semua field dengan tanda * kemudian klik lanjutkan</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-bni/4.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Kemudian akan muncul layar konfirmasi</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-bni/5.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Pastikan No Va & Nama VA sesuai<br>
                                    Input BNI E-Secure chalenge pada token Anda<br>
                                    Input BNI E- Secure respon ke layar internet banking dan proses
							</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/internet-banking-bni/6.jpg" width="350"></td>
                    </tr>
                    </table>                    
                </div>


            </div>



        </div>

        <div class="clear"></div>

    </div>

</section>

<!-- end of middle -->

<?php include('inc_footer.php'); ?>