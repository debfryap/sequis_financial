<?php $page = "our_product"; ?>

<?php include('inc_header.php'); ?>

<!-- middle -->

<section>

    <div class="wrapper">

        <div id="banner-content"><img src="images/slider/banner-product.jpg" alt="Our products"></div>

        <nav class="share">

            <div class="left">Share:

                <a href="#"><img src="images/material/nav-tw.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-fb.png" alt=""></a>

                <a href="#"><img src="images/material/nav-mail.png" alt=""></a>

            </div>

            <div class="right">

                <a href="#"><img src="images/material/nav-zoomin.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-zoomout.png" alt=""></a>

                <a href="#"><img src="images/material/nav-print.png" alt=""></a>

            </div>

        </nav>

        <aside>

            <ul class="submenu">

                <li><a href="product-01.php">Group Life</a></li>

                <li><a href="product-02.php">Group Health</a></li>

                <li><a href="product-03.php" class="active">Group Saving</a></li>
                
            </ul>

            <div class="side_link ">

                <div class="label">sequis link</div>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid1.png" alt="my sequis" /></span>

                    <span class="text">

                        <h6>my Sequis</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid2.png" alt="Sequisfriend"/></span>

                    <span class="text">

                        <h6>Sequisfriend</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid3.png" alt="Calculator"/> </span>

                    <span class="text">

                        <h6>Calculator</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

            </div>

            <address>

                <div class="label">get in touch</div>

                <div>

                    <img src="images/material/icon-pointer.png" alt="">

                    <h6>Sequis Group</h6>

                    <p>Sequis Center Lt. 5<br />

                        Jl. Jend. Sudirman No. 71<br />

                        Jakarta 12190, Indonesia<br />

                        T. +6221 5226 677<br />

                        F. +6221 5205 837

                    </p>

                </div>

                <a href="#">Get Direction</a> <a href="#">Send Message</a>

            </address>

        </aside>

        <div id="content">

            <nav class="breadcumb"><a href="#">Home</a> / <a href="#">Our Products </a> / <a href="#">Group Saving</a></nav>

            <h2>Group Saving</h2>

            <div class="img_content">

                <img src="images/content/img-about.jpg" alt="">

            </div>

            <p>Merupakan tabungan hari tua karyawan dengan basis asuransi jiwa.</p>
              <ul class="ul_style">
                <li>Group Universal Life<br />
                  Tabungan hari tua karyawan berbasis asuransi jiwa yang memiliki manfaat perlindungan asuransi 24 <br />
                  jam di seluruh dunia dan 100 % uang pertanggungan dibayarkan secara tunai.</li>
                  <li>Sequis Severance Program (SSP)</li>
              </ul>

              <h6>Asuransi Pendidikan</h6>
              <p>Asuransi pendidikan dari Sequis Financial untuk membantu kepastian kelangsungan dana pendidikan anak terdiri dari dua produk layanan.</p>
              <ul class="ul_style">
                <li>        Edu Plus<br />
                  Merupakan asuransi pendidikan dari Sequis Life yang memberi manfaat kelangsungan dana <br />
                  pendidikan anak dengan dana manfaat hidup 150% dari uang pertanggungan, 300% dari uang <br />
                  pertanggungan jika meninggal dunia karena sakit dan 350% dari uang pertanggungan jika <br />
                  meninggal karena kecelakaan.        </li>
                  <li> Smart Kids Plan<br />
                      Merupakan asuransi pendidikan yang memberikan kelangsungan dana pendidikan anak dengan <br />
                      manfaat hidup 150% dari uang pertanggungan jika meninggal, 200% dari uang pertanggungan jika <br />
                      meninggal dunia karena kecelakaan.</li>
                  </ul>
                  <h6>Asuransi Kesehatan</h6>
                  <p>Asuransi kesehatan dari Sequis Financial yang memberikan jaminan kesehatan berupa perawatan di rumah sakit maupun perlindungan terhadap penyakit kritis.</p>
                  <ul class="ul_style">
                    <li>      Hospital Care Plus<br />
                      Merupakan asuransi kesehatan yang memberikan jaminan perawatan menginap dan layanan <br />
                      darurat kesehatan yang berlaku di seluruh rumah sakit dalam dan luar negeri. </li>
                      <li> Critical Illness Care<br />
                          Merupakan asuransi perlindungan hingga usia 65 tahun terhadap 36 jenis penyakit kritis dengan <br />
                          jaminan uang pertanggungan sebesar 350% dari premi yang dibayarkan. </p>
                      </li>
                  </ul>
                  <h6>Asuransi Kecelakaan</h6>
                  <p>Asuransi kecelakaan yang memberikan jaminan kepastian dana pada biaya perawatan yang disebabkan oleh kecelakaan.</p>
                  <ul class="ul_style">
                    <li>Personal Accident<br />
                      Asuransi kecelakaan yang memberikan manfaat harian rumah sakit yang meliputi penggantian <br />
                      biaya perawatan karena kecelakaan serta manfaat TPD dan PPD dari uang pertanggungan yang <br />
                      akan dibayarkan jika terbukti disebabkan oleh kecelakaan. </li>
                      <li>ACB</li>
              </ul>
              <h6>Whole Life</h6>
              <p>Merupakan asuransi yang memberikan perlindungan jiwa seumur hidup.</p>
              <ul class="ul_style">
                  <li>  Q Smart Life</li>
              </ul>
              <p>Asuransi yang memberikan perlindungan jiwa seumur hidup dengan manfaat 700% dari premi yang dibayarkan jika peserta meninggal atau hidup hingga akhir perlindungan (mencapai usia 100 tahun).</p>
              <h6>Term Life</h6>
              <ul class="ul_style">
                <li>Protection Plus</li>
            </ul>
            <p>Asuransi dengan manfaat perlindungan 100% dari uang pertanggungan jika peserta meninggal dan 200% dari uang pertanggungan jika peserta meninggal karena kecelakaan. </p>


        </div>

        <div class="clear"></div>

    </div>

</section>

<!-- end of middle -->

<?php include('inc_footer.php'); ?>