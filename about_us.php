<?php $page = "about"; ?>

<?php include('inc_header.php'); ?>

<!-- middle -->

<section>

    <div class="wrapper">

        <div id="banner-content"><img src="images/slider/banner-about.jpg" alt="About us"></div>

        <nav class="share"><div class="left">Share:

                <a href="#"><img src="images/material/nav-tw.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-fb.png" alt=""></a>

                <a href="#"><img src="images/material/nav-mail.png" alt=""></a></div>

            <div class="right"><a href="#"><img src="images/material/nav-zoomin.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-zoomout.png" alt=""></a>

                <a href="#"><img src="images/material/nav-print.png" alt=""></a></div>

        </nav>

        <aside>

            <div class="side_link ">

                <div class="label">sequis link</div>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid1.png" alt="my sequis" /></span>

                    <span class="text">

                        <h6>my Sequis</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid2.png" alt="Sequisfriend"/></span>

                    <span class="text">

                        <h6>Group Health</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid3.png" alt="Calculator"/> </span>

                    <span class="text">

                        <h6>Group Saving</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

            </div>

            <address>

                <div class="label">get in touch</div>

                <div>

                    <img src="images/material/icon-pointer.png" alt="">

                    <h6>Sequis financial</h6>

                    <p>Sequis Center Lt. 5<br />

                        Jl. Jend. Sudirman No. 71<br />

                        Jakarta 12190, Indonesia<br />

                        T. +6221 5226 677<br />

                        F. +6221 5205 837

                    </p>

                </div>

                <a href="#">Get Direction</a> <a href="#">Send Message</a>

            </address>

        </aside>

        <div id="content">

            <nav class="breadcumb"><a href="#">Home</a> / <a href="#">About Us</a></nav>

            <h2>About US</h2>

            <div class="img_content">

                <img src="images/content/img-about.jpg" alt="">

            </div>

            <p>Karyawan merupakan salah satu aset terpenting perusahaan yang harus senantiasa diperhatikan kesejahteraan dalam bekerja. Inilah mengapa Sequis Financial dengan lebih dari satu dekade pengalaman, menghadirkan Program Kesejahteraan Karyawan atau Employee Benefit Business. Program ini terdiri dari berbagai produk dan layanan asuransi yang inovatif, untuk melindungi dan memberi rasa aman kepada setiap karyawan serta perusahaan.</p>
<p>Employee Benefit Business memiliki tujuan untuk meraih target penjualan melalui aktivitas direct sales, referral, broker serta dengan membangun hubungan baik kepada nasabah, agensi dan rekanan. Selain itu juga didorong untuk selalu berinisiatif dalam menciptkan program-program efektif demi menawarkan produk asuransi dan tabungan bagi korporasi. <br />
</p>
<p>Beberapa klien kami:</p>  

        </div>

        <div class="clear"></div>

    </div>

</section>

<!-- end of middle -->

<?php include('inc_footer.php'); ?>