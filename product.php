<?php $page = "our_product"; ?>

<?php include('inc_header.php'); ?>

<!-- middle -->

<section>

    <div class="wrapper">

        <div id="banner-content"><img src="images/slider/banner-product.jpg" alt="Our products"></div>

        <nav class="share">

            <div class="left">Share:

                <a href="#"><img src="images/material/nav-tw.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-fb.png" alt=""></a>

                <a href="#"><img src="images/material/nav-mail.png" alt=""></a>

            </div>

            <div class="right">

                <a href="#"><img src="images/material/nav-zoomin.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-zoomout.png" alt=""></a>

                <a href="#"><img src="images/material/nav-print.png" alt=""></a>

            </div>

        </nav>

        <aside>

            <ul class="submenu">

                <li><a href="product-01.php" class="active">Group Life</a></li>

                <li><a href="product-02.php">Group Health</a></li>

                <li><a href="product-03.php">Group Saving</a></li>
                
            </ul>

            <div class="side_link ">

                <div class="label">sequis link</div>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid1.png" alt="my sequis" /></span>

                    <span class="text">

                        <h6>my Sequis</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid2.png" alt="Sequisfriend"/></span>

                    <span class="text">

                        <h6>Sequisfriend</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid3.png" alt="Calculator"/> </span>

                    <span class="text">

                        <h6>Calculator</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

            </div>

            <address>

                <div class="label">get in touch</div>

                <div>

                    <img src="images/material/icon-pointer.png" alt="">

                    <h6>Sequis Group</h6>

                    <p>Sequis Center Lt. 5<br />

                        Jl. Jend. Sudirman No. 71<br />

                        Jakarta 12190, Indonesia<br />

                        T. +6221 5226 677<br />

                        F. +6221 5205 837

                    </p>

                </div>

                <a href="#">Get Direction</a> <a href="#">Send Message</a>

            </address>

        </aside>

        <div id="content">

            <nav class="breadcumb"><a href="#">Home</a> / <a href="#">Our Products </a> / <a href="#">Life Protection </a></nav>

            <h2>Our Product</h2>

            <div class="img_content">

                <img src="images/content/img-about.jpg" alt="">

            </div>

            <p><strong>Group Term Life</strong><br />
Program Asuransi Jiwa Kumpulan atau Group Term Life merupakan perlindungan yang mencakup resiko kematian oleh sebab apapun. Bisa dikombinasikan dengan perlindungan dari resiko kecelakaan yang mengakibatan cacat tetap permanent (TPD), kecelakaan yang mengakibatkan kematian ( ADD), serta resiko terdiagnosa 36 penyakit kritis ( CI ) dengan keuntungan uang pertanggungan ganda*. </p>
<p>*Sistem pertanggungan sesuai dengan kebijakan perusahaan atau sesuai dengan UUTK 13/2003<br />
</p>
<p><strong>Group Health</strong><br />
  Program Kesehatan Kumpulan atau Group Health merupakan perlindungan berbasis asuransi jiwa dengan manfaat penanganan perawatan di rumah sakit (IP), Program Maternity bagi karyawan termasuk keluarga yang bisa ditambahkan dengan fasilitas ASO (Adminitrastive Services Only), serta I-Assist International yang memudahkan peserta asuransi dalam mendapat penanganan di dalam maupun luar negeri. <br />
</p>
<p><strong>Saving</strong><br />
Saving atau tabungan dari Sequis Financial merupakan bagian dari Program Kesejahteraan yang mengakomodasi Undang-Undang No. 13 Tahun 2013 mengenai kewajiban perusahaan terhadap karyawan dalam bentuk Pesangon.</p>
<p>Layanan ini selain memberikan manfaat sebagai pengurang pajak badan (Pph 25), dapat dikombinasikan dengan manfaat untuk perlindungan jiwa, termasuk juga digabungkan menjadi dana investasi pada instrument investasi konservatif seperti  Sertifikat Bank Indonesia, Deposito, dan Obligasi (GUL) maupun investasi yang bersifat lebih agresif semacam investasi di pasar modal yang lebih dinamis (SSP).<br />
</p>


        </div>

        <div class="clear"></div>

    </div>

</section>

<!-- end of middle -->

<?php include('inc_footer.php'); ?>