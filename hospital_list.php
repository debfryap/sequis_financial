<?php $page = "our_customers"; ?>

<?php include('inc_header.php'); ?>

<!-- middle -->

<section>

    <div class="wrapper">

        <div id="banner-content"><img src="images/slider/banner-ourcust.jpg" alt="Our Customers"></div>

        <nav class="share">

            <div class="left">Share:

                <a href="#"><img src="images/material/nav-tw.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-fb.png" alt=""></a>

                <a href="#"><img src="images/material/nav-mail.png" alt=""></a>

            </div>

            <div class="right">

                <a href="#"><img src="images/material/nav-zoomin.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-zoomout.png" alt=""></a>

                <a href="#"><img src="images/material/nav-print.png" alt=""></a>

            </div>

        </nav>

        <aside>

            <ul class="submenu">

                <li><a href="#" class="parent">Process Claim</a>
                	<ul>
                        <li><a href="process_claim_1.php">Pengajuan Proses Klaim </a></li>
                        <li><a href="process_claim_2.php">Pengajuan Pembayaran Klaim Meninggal Dunia </a></li>
                        <li><a href="process_claim_3.php">Pengajuan Klaim (selain klaim meninggal dunia) </a></li>
                        
                    </ul>
                </li>

                <li>  

                    <a href="#" class="parent">Payment Methods</a>

                    <ul>

                        <li><a href="payment_method_1.php">Bank Transfer</a></li>

                        <li><a href="payment_method_2.php">Internet Banking</a></li>

                        <li><a href="payment_method_3.php">Mobile Banking</a></li>

                        <li><a href="payment_method_4.php">SMS Banking</a></li>

                        <li><a href="payment_method_5.php">Auto Debet</a></li>

                    </ul>

                </li>

                <li>

                    <a href="#" class="parent">Panduan Layanan</a>

                    <ul>

                        <li><a href="panduan_layanan_1.php">Pembatalan Pembayaran Premi Melalui Pendebetan Rekening/Kartu Kredit</a></li>

                        <li><a href="panduan_layanan_2.php">Penarikan Nilai Tunai/Dana Investasi</a></li>

                        <li><a href="panduan_layanan_3.php">Pemulihan Polis</a></li>

                        <li><a href="panduan_layanan_4.php">Pengajuan Transaksi Unit Link</a></li>

                        <li><a href="panduan_layanan_5.php">Pengajuan Pinjaman Polis</a></li>

                        <li><a href="panduan_layanan_6.php">Pengajuan Perubahan Polis</a></li>

                        <li><a href="panduan_layanan_7.php">Pengajuan Duplikat Polis dan Kartu Kesehatan</a></li>

                        <li><a href="panduan_layanan_8.php">Penarikan Manfaat Tunai dan Deviden</a></li>

                        <li><a href="panduan_layanan_9.php">Pengajuan Bankers Clause / Klausula Ban</a></li>

                    </ul>

                </li>

                <li><a href="hospital_list.php " class="active">Hospital List</a></li>

            </ul>



            <div class="side_link ">

                <div class="label">sequis link</div>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid1.png" alt="my sequis" /></span>

                    <span class="text">

                        <h6>my Sequis</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid2.png" alt="Sequisfriend"/></span>

                    <span class="text">

                        <h6>Sequisfriend</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid3.png" alt="Calculator"/> </span>

                    <span class="text">

                        <h6>Calculator</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

            </div>

            <address>

                <div class="label">get in touch</div>

                <div>

                    <img src="images/material/icon-pointer.png" alt="">

                    <h6>Sequis Group</h6>

                    <p>Sequis Center Lt. 5<br />

                        Jl. Jend. Sudirman No. 71<br />

                        Jakarta 12190, Indonesia<br />

                        T. +6221 5226 677<br />

                        F. +6221 5205 837

                    </p>

                </div>

                <a href="#">Get Direction</a> <a href="#">Send Message</a>

            </address>

        </aside>

        <div id="content">

            <nav class="breadcumb"><a href="#">Home</a> / <a href="#">Our Customers </a> / <a href="#">Payment Methods</a> / <a href="#">Hospital List</a></nav>

            <h2>Hospital List</h2>



            <p>

                Pemegang polis dapat menunjuk pihak bank sebagai penerima manfaat klaim meninggal jika terjadi resiko atas diri tertanggung sehubungan dengan fasilitas pinjaman di bank oleh tertanggung.

            </p>



            <h5 class="black">Find a Hospital:</h5>



            <div class='search_hospital'>

                <div class="row">

                    <label>Institution Name:</label>

                    <select name="select" id="select" class="fm_select">

                        <option>All</option>

                        <option>Choose two</option>

                        <option>Choose three</option>

                        <option>Choose four</option>

                    </select>

                </div>

                <div class="row">

                    <div class="column">

                        <label>City:</label>

                        <select name="select" id="select" class="fm_select">

                            <option>All</option>

                            <option>Choose two</option>

                            <option>Choose three</option>

                            <option>Choose four</option>

                        </select>

                        <input type="submit" value="search" class='button'/>

                    </div>

                </div>

            </div>



            <h6>DAFTAR RUMAH SAKIT REKANAN SEQUISLIFE</h6>

            <br/><br/>

            <div class="box_hospital">

                <h6>CEMPAKA AZ-ZAHRA</h6>

                <p>Jl. Pocut Baren No. 36-40 Kp. Laksana Kec. Kuta Alam Banda Aceh<br/>

                    Phone : 651- 31066 </p>

            </div>

            <div class="box_hospital">

                <h6>SILOAM HOSPITALS BALIKPAPAN (RS BALIKPAPAN HUSADA)</h6>

                <p>Jl. MT Haryono No. 9 Ringroad Balikpapan Kalimantan Timur <br/>

                    Phone :  0542-7206508/09/12</p>

            </div>

            <div class="box_hospital">

                <h6>ADVENT BANDUNG </h6>

                <p>Jl Cihampelas, No.161<br/>

                    Phone :  022 -2034386-9</p>

            </div>

            <div class="box_hospital">

                <h6>HERMINA BEKASI </h6>

                <p>Jl. Kemakmuran No.39, Margajaya<br/>

                    Phone :  021-8842121/3</p>

            </div>

            <div class="box_hospital">

                <h6>BOGOR MEDICAL CENTER </h6>

                <p>Jl. Pajajaran Indah V/97 Bogor<br/>

                    Phone :  0251-8390619/8390435/8346002/8379743|0251-346002 </p>

            </div>

            <div class="box_hospital">

                <h6>SILOAM HOSPITALS KEBON JERUK </h6>

                <p>Jl. Raya Perjuangan, Kav.8, Kebon Jeruk <br/>

                    Phone :   021 - 5300888/ 021 - 5300889/021 - 53662997/021 - 5300889 </p>

            </div>

            <div class="box_hospital">

                <h6>ROYAL PRIMA JAMBI</h6>

                <p>Jl. Raden WIijaya RT 035 Kel. Thehok Jambi<br/>

                    Phone :   0741-41010</p>

            </div>

            <div class="box_hospital">

                <h6>MUHAMMADIYAH AHMAD DAHLAN</h6>

                <p>Jl. Gatot Subroto No 84 Kediri<br/>

                    Phone :   0354-773115/776182/770182</p>

            </div>

            <div class="box_hospital">

                <h6>COLUMBIA ASIA MEDAN (d/h GLENI MEDAN) </h6>

                <p>Jl. Listrik 2A Medan 20112<br/>

                    Phone :   061-4566368 </p>

            </div>





            <br/><br/>



            <div class="paging"><a href="#" class="active">1</a> <a href="#">2</a> <a href="#">3</a> <a href="#">4</a> <a href="#">5</a></div>



            <br/><br/>

            <br/><br/>

            <a href="" class="std_link pdf">

                <span class="text">Step by Step Payment Methods</span>

                <span class="file">DOC files</span>

            </a>

            

            <div class="clear"></div>

        </div>

</section>

<!-- end of middle -->

<?php include('inc_footer.php'); ?>