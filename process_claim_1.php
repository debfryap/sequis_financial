<?php $page = "our_customers"; ?>

<?php include('inc_header.php'); ?>

<!-- middle -->

<section>

    <div class="wrapper">

        <div id="banner-content"><img src="images/slider/banner-ourcust.jpg" alt="Our Customers"></div>

        <nav class="share">

            <div class="left">Share:

                <a href="#"><img src="images/material/nav-tw.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-fb.png" alt=""></a>

                <a href="#"><img src="images/material/nav-mail.png" alt=""></a>

            </div>

            <div class="right">

                <a href="#"><img src="images/material/nav-zoomin.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-zoomout.png" alt=""></a>

                <a href="#"><img src="images/material/nav-print.png" alt=""></a>

            </div>

        </nav>

        <aside>

            <ul class="submenu">
                <li class="expand"><a href="#" class="parent">Process Claim</a>
                	<ul>
                        <li><a href="process_claim_1.php" class="active">Proses Klaim Kesehatan </a></li>
                        <li><a href="process_claim_2.php">Pengajuan Pembayaran Klaim Meninggal Dunia </a></li>
                        <li><a href="process_claim_3.php">Pengajuan Klaim (selain klaim meninggal dunia) </a></li>
                        
                    </ul>
                </li>
                <li>  
                    <a href="#" class="parent">Payment Methods</a>
                    <ul>
                        <li><a href="payment_method_1.php">Bank Transfer</a></li>
                        <li><a href="payment_method_6.php">ATM</a></li>
                        <li><a href="payment_method_2.php">Internet Banking</a></li>
                        <li><a href="payment_method_3.php">Mobile Banking</a></li>
                        <!--<li><a href="payment_method_4.php">SMS Banking</a></li>-->
                        <li><a href="payment_method_5.php">Auto Debet</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="parent">Panduan Layanan</a>
                    <ul>
                        <li><a href="panduan_layanan_1.php">Pembatalan Pembayaran Premi Melalui Pendebetan Rekening/Kartu Kredit</a></li>
                        <li><a href="panduan_layanan_2.php">Penarikan Nilai Tunai/Dana Investasi</a></li>
                        <li><a href="panduan_layanan_3.php">Pemulihan Polis</a></li>
                        <li><a href="panduan_layanan_4.php">Pengajuan Transaksi Unit Link</a></li>
                        <li><a href="panduan_layanan_5.php">Pengajuan Pinjaman Polis</a></li>
                        <li><a href="panduan_layanan_6.php">Pengajuan Perubahan Polis</a></li>
                        <li><a href="panduan_layanan_7.php">Pengajuan Duplikat Polis dan Kartu Kesehatan</a></li>
                        <li><a href="panduan_layanan_8.php">Penarikan Manfaat Tunai dan Deviden</a></li>
                        <li><a href="panduan_layanan_9.php">Pengajuan Bankers Clause / Klausula Ban</a></li>
                    </ul>
                </li>
                <li><a href="hospital_list.php">Hospital List</a></li>
            </ul>

            <div class="side_link ">

                <div class="label">sequis link</div>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid1.png" alt="my sequis" /></span>

                    <span class="text">

                        <h6>my Sequis</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid2.png" alt="Sequisfriend"/></span>

                    <span class="text">

                        <h6>Sequisfriend</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid3.png" alt="Calculator"/> </span>

                    <span class="text">

                        <h6>Calculator</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

            </div>

            <address>

                <div class="label">get in touch</div>

                <div>

                    <img src="images/material/icon-pointer.png" alt="">

                    <h6>Sequis Group</h6>

                    <p>Sequis Center Lt. 5<br />

                        Jl. Jend. Sudirman No. 71<br />

                        Jakarta 12190, Indonesia<br />

                        T. +6221 5226 677<br />

                        F. +6221 5205 837

                    </p>

                </div>

                <a href="#">Get Direction</a> <a href="#">Send Message</a>

            </address>

        </aside>

        <div id="content">

            <nav class="breadcumb"><a href="#">Home</a> / <a href="#">Our Customers </a> / <a href="#">Process Claim</a>/ <a href="#">Proses Klaim Kesehatan</a></nav>

            <h2>Proses Klaim Kesehatan</h2>
			
            <div class="accordion">
                <div class="head">
                    Klaim Cashless
                </div>
                <div class="content">
                    <h6>Prosedur Klaim Cashless</h6>
                    <p>Merupakan fasilitas kemudahan yang diberikan ketika Anda dirawat di rumah sakit rujukan atau rekanan dari Admedika tanpa perlu membayar tunai namun cukup dengan menunjukan kartu peserta dan kartu identitas kepada petugas pendaftaran.</p>
                    <p><img src="images/content/Flow_Process_Claim_Cashless.jpg"</p>                    
                   
                </div>

                <div class="head">
                    Klaim Penggantian Biaya (Reimbursement)
                </div>
                <div class="content">
                <p>Merupakan fasilitas yang disediakan Sequislife untuk memudahkan Tertanggung ketika menjalani rawat jalan dan rawat inap di seluruh rumah sakit yang diajukan paling lambat 30 hari setelah Tertanggung keluar dari rumah sakit.</p>
                    <h6>Prosedur Klaim Penggantian Biaya</h6>
                    <p>
                    <ul class="ul_style">
                    	<li>Mengisi dan menandatangani formulir pengajuan klaim perawatan RS</li>
                        <li>Menyerahkan Surat Keterangan Dokter (SKD)</li>
                        <li>Fotokopi identitas Tertanggung yang berlaku</li>
                        <li>Melampirkan kuitansi dan rincian tagihan asli yang dikeluarkan oleh rumah sakit Apabila 2 (dua) dokumen asli tersebut telah diserahkan kepada pihak Penanggung lain maka dapat diganti dengan fotokopi dokumen yang telah dilegalisir oleh rumah sakit disertai dengan surat rincian pembayaran dari Penanggung Lain tersebut</li>
                        <li>Melampirkan rincian obat-obatan dari rumah sakit yang diresepkan dan dikonsumsi selama perawatan termasuk apabila berobat di luar negeri</li>
                        <li>Menyerahkan hasil pemeriksaan yang dilakukan selama perawatan (laboratorium, rontgen, patologi anatomi, EKG dan lain-lain)</li>
                        <li>Surat keterangan/ dokumen lainnya yang mungkin diperlukan</li>

                    </ul>
                    <br></p>
                    <h6>Prosedur Klaim Penggantian Biaya Akibat Kecelakaan</h6>                   
                    
                    <p>Menyerahkan dokumen di atas dengan tambahan dokumen antara lain:</p>
                    <ul class="ul_style">
                        <li>Melampirkan Surat Kecelakaan dari kepolisian bila kecelakaan lalu lintas dan dari tempat bekerja bila kecelakaan kerja</li>
                        <li>Surat keterangan/ dokumen lainnya yang mungkin diperlukan</li>
                    </ul>
                    <br>
                    <h6>Prosedur Pengajuan Klaim Kesehatan Setelah Rawat Inap</h6>
                    <ul class="ul_style">
                        <li>Melampirkan kuitansi pembelian obat-obat disertai dengan copy resep.</li>
                        <li>Mengisi Formulir Pengajuan Klaim dan Surat Keterangan Dokter</li>
                    </ul>
                    
                <div class="download_file">
                <h6>Formulir  :</h6>
                    <div class="row">
                        <a href="" class="doc">
                            <span class="text">Pengajuan Klaim</span>
                            <span class="file">DOC files</span>
                        </a>
                    </div>
                    <div class="row">
                        <a href="" class="doc">
                            <span class="text">Surat Keterangan Dokter</span>
                            <span class="file">DOC files</span>
                        </a>
                    </div>
                    <div class="row">
                        <a href="" class="doc">
                            <span class="text">Surat Keterangan Ahli Waris</span>
                            <span class="file">DOC files</span>
                        </a>
                    </div>
            	</div>
                    
                </div>
            </div>



        </div>

        <div class="clear"></div>

    </div>

</section>

<!-- end of middle -->

<?php include('inc_footer.php'); ?>