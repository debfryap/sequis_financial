<?php $page = "contact"; ?>
<?php include('inc_header.php'); ?>
<!-- middle -->
<section>
    <div class="wrapper">
        <div id="banner-content"><img src="images/slider/banner-contact.jpg" alt="Contact us"></div>
        <nav class="share">
            <div class="left">Share: <a href="#"><img src="images/material/nav-tw.png" alt=""></a> <a href="#" class="center"><img src="images/material/nav-fb.png" alt=""></a> <a href="#"><img src="images/material/nav-mail.png" alt=""></a></div>
            <div class="right"><a href="#"><img src="images/material/nav-zoomin.png" alt=""></a> <a href="#" class="center"><img src="images/material/nav-zoomout.png" alt=""></a> <a href="#"><img src="images/material/nav-print.png" alt=""></a></div>
        </nav>
        <aside>            
            <address>
                <div class="label">get in touch</div>
                <div><img src="images/material/icon-pointer.png" alt="">
                    <h6>Sequis Group</h6>
                    <p>Sequis Center Lt. 5<br />
                        Jl. Jend. Sudirman No. 71<br />
                        Jakarta 12190, Indonesia<br />
                        T. +6221 5226 677<br />
                        F. +6221 5205 837</p>
                </div>
                <a href="#">Get Direction</a> <a href="#">Send Message</a>
            </address>
        </aside>
        <div id="content">
            <nav class="breadcumb"><a href="#">Home</a> / <a href="#">Contact Us</a> / <a href="#">Enquiries</a></nav>
            <h2>Enquiries</h2>
            <h3>Do you have any question? Please contact us.</h3>
            <form action="" method="post" id="contact">
                <table width="100%" border="0">
                    <tr>
                        <td><label for="textfield">Name <span>*</span></label>
                            <input type="text" name="textfield" id="textfield" class="name"></td>
                    </tr>
                    <tr>
                        <td>
                            <label for="textfield2">Email <span>*</span></label><input type="text" name="textfield2" id="textfield2" class="name"></td>
                    </tr>
                    <tr>
                        <td><label for="textfield3">Subject <span>*</span></label><input type="text" name="textfield3" id="textfield3" class="subject"></td>
                    </tr>
                    <tr>
                        <td><label for="select">Send To <span>*</span></label>
                            <select name="select" id="select" class="fm_select">
                                <option>Choose One</option>
                                <option>Choose two</option>
                                <option>Choose three</option>
                                <option>Choose four</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td><label for="textfield5">Message <span>*</span></label><textarea name="textfield5" id="textfield5"></textarea></td>
                    </tr>
                    <tr>
                        <td><label for="textfield4">Security Code <span>*</span></label>
                            <img src="images/content/captcha.jpg" alt=""></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="textfield4" id="textfield4" class="code"></td>
                    </tr>
                    <tr>
                        <td><button type="submit" class="btn-blue">Send message</button></td>
                    </tr>
                </table>
            </form>      
        </div>
        <div class="clear"></div>
    </div>
</section>
<!-- end of middle -->
<?php include('inc_footer.php'); ?>