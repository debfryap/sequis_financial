$(document).ready(function () {
    /*custom select*/
    $(".lang").each(function (index, element) {
        var width = $(this).outerWidth();
        var value = $(this).children('option:first').text();

        $(this).wrap("<div class=selector_form_lang>");
        $(".selector_form_lang").eq(index).css('width', width + 'px').prepend("<span>" + value + "</span>");
        $(this).on('change', function () {
            var value = $(this).children(":selected").text();
            if (value.length >= 20) {
                $(this).siblings("span").text(value.substring(0, 20) + "..");
            } else {
                $(this).siblings("span").text(value);
            }
        });
    });

    /* caption banner content*/
    $("body").find("#banner-content").append("<div class='title'><span>" + $("body").find("#banner-content").find("img").attr("alt") + "</span></div><div class='triangel'></div>");

    $('.fancybox').fancybox({
        afterLoad: function () {
            this.title = (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '' + ' Images ');
        }
    });
    $('.inline').fancybox();

    /*custom select*/
    $(".fm_select").each(function (index, element) {
        var width = $(this).outerWidth();
        var value = $(this).children('option:first').text();

        $(this).wrap("<div class=selector_form>");
        $(".selector_form").eq(index).css('width', width + 'px').prepend("<span>" + value + "</span>");
        $(this).on('change', function () {
            var value = $(this).children(":selected").text();
            if (value.length >= 20) {
                $(this).siblings("span").text(value.substring(0, 20) + "..");
            } else {
                $(this).siblings("span").text(value);
            }
        });
    });
    $(".file_form input").on("change", function () {
        var val = $(this).val();
        var filename = val.split("\\").pop();
        $(this).siblings("div").text(filename);
    });
    $(".accordion li:first a").addClass('active');
    $(".accordion li:first .answer").show();
    $(".accordion li a").click(function (e) {
        $(this).toggleClass("active");
        $(this).siblings(".answer").slideToggle(500);
        return false;
    });

    submenuDrop()// submenu dropdown (sidebar)
    accordion(); //accordion 
});

function submenuDrop() {
    $("aside .submenu li a").on('click', function (e) {
        var elem = $(this);
        if (elem.next("ul").length) {
            e.preventDefault();
            if (elem.next('ul').is(":visible")) {
                elem.next('ul').slideUp(300);
            } else {
                elem.next('ul').slideDown(300);
            }
        }

    });
}
function accordion() {
    var elem = $(".accordion");
    elem.children('.head').each(function () {
        if ($(this).hasClass('expand')) {
            $(this).addClass('expand');
            $(this).next('.content').slideDown(300);
        } else {
            $(this).removeClass('expand');
            $(this).next('.content').slideUp(300);
        }
    });
    elem.find('.head').click(function () {
        if ($(this).hasClass('expand')) {
            $(this).removeClass('expand');
            $(this).next('.content').slideUp(300);
        } else {
            $(this).addClass('expand');
            $(this).next('.content').slideDown(300);
        }
    });
}