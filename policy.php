<?php $page = "our_product"; ?>

<?php include('inc_header.php'); ?>

<!-- middle -->

<section>

    <div class="wrapper">

        <div id="banner-content"><img src="images/slider/banner-desclaimer.jpg" alt="Disclaimer"></div>

        <nav class="share">

            <div class="left">Share:

                <a href="#"><img src="images/material/nav-tw.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-fb.png" alt=""></a>

                <a href="#"><img src="images/material/nav-mail.png" alt=""></a>

            </div>

            <div class="right">

                <a href="#"><img src="images/material/nav-zoomin.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-zoomout.png" alt=""></a>

                <a href="#"><img src="images/material/nav-print.png" alt=""></a>

            </div>

        </nav>

        <aside>

             <div class="side_link ">

                <div class="label">sequis link</div>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid1.png" alt="my sequis" /></span>

                    <span class="text">

                        <h6>my Sequis</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid2.png" alt="Sequisfriend"/></span>

                    <span class="text">

                        <h6>Sequisfriend</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid3.png" alt="Calculator"/> </span>

                    <span class="text">

                        <h6>Calculator</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

            </div>

            <address>

                <div class="label">get in touch</div>

                <div>

                    <img src="images/material/icon-pointer.png" alt="">

                    <h6>Sequis Group</h6>

                    <p>Sequis Center Lt. 5<br />

                        Jl. Jend. Sudirman No. 71<br />

                        Jakarta 12190, Indonesia<br />

                        T. +6221 5226 677<br />

                        F. +6221 5205 837

                    </p>

                </div>

                <a href="#">Get Direction</a> <a href="#">Send Message</a>

            </address>

        </aside>

        <div id="content">

            <nav class="breadcumb"><a href="#">Home</a> / <a href="#">Our Products </a> / <a href="#">Life Protection </a></nav>

            <h2>Privacy Policy</h2>

            <p><strong>Please read this Privacy Policy Carefully. By accessing this Website you accept all the terms and conditions of this Privacy Policy.</strong></p>
<p>We will require your consent for the use and disclosure of your personal data. By submitting your personal data to us online in our website, you agree to give us consent to use and disclose your personal data in accordance with this policy. If you do not agree with the terms and conditions of this Privacy Policy, you must immediately cease using and accessing this Website and any content, services or products contained in or available from this Website for which you need to provide personal information.</p>
<p><strong>TERMS &amp; CONDITIONS</strong></p>
<p><strong>1. Scope of this Policy</strong><br />
  This Privacy Policy covers the collection, use and disclosure of personal information that is collected through or in connection with the Sequislife website including all web pages related to the website but excluding any links to third party sites (the &quot;Website&quot;).<br />
</p>
<p><strong>2. Collection and Use of Personal Information</strong><br />
  Except as set out in this Privacy Policy, we will not disclose any personally identifiable information without your permission unless we are legally entitled or required to do so (for example, if required to do so by legal process or for the purposes of prevention of fraud or other crime) or if we believe that such action is necessary to protect and/or defend our rights, property or personal safety and those of our users/customers etc.<br />
</p>
<p>We collect and use your personal information (including names, addresses, telephone numbers and email addresses) for the purposes of:</p>
<ul>
  <li> responding to queries or requests submitted by you</li>
  <li> administering or otherwise carrying out our obligations in relation to any agreement you have with us</li>
  <li> to improve the Website (we continually strive to improve Website offerings based on any information <br />
  or feedback received from you)</li>
  <li> Internal record keeping</li>
  <li> informing you of any events, services and possible job opportunities<br />
  </li>
</ul>
<p><strong>3. Maintaining the accuracy of our records </strong></p>
<p>We aim to keep our information about you as accurate as possible. Please contact us to notify us of any changes to personal information you have provided to us that you would like to be made.</p>
<p><strong>4. Security of your personal data</strong><br />
Sequislife has implemented policies, procedures and technology to protecting your privacy from unauthorized access and improper use. These will be updated as new technology becomes available, as appropriate.</p>
<p><strong>5. Cookies</strong><br />
A cookie is a small file that is sent to your computer when you visit a website. When you visit the website again, the cookie allows that site to recognize your browser. Cookies may store user preferences and other information. You can reset your browser to refuse all cookies or to indicate when a cookie is being sent. We may store a cookie on your computer when you look at our Website. We are able to read these cookies for information purposes when you revisit our Website.</p>
<p>The type of information collected from a cookie is specific to the PC used to access the Website, and includes the IP address, the date and time the PC visited the Website, what parts of our Website were looked at and whether the web pages requested were delivered successfully. This information is anonymous; it represents a computer rather than a person. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us.</p>
<p>The cookie information may be used by Sequislife to improve our understanding about the use of the Website and to improve its operation and functionality.</p>
<p>You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer.<br />
</p>
<p><strong>6. Changes to the Privacy Policy</strong></p>
<p>This Privacy Policy may change from time to time and if such changes substantially impact the collection, use and disclosure of your personal information, RGE will post those changes on this page in a manner that identifies that a change has taken place. Please check this Privacy Policy on a regular basis.</p>
<p><strong>CONDITIONS OF USE</strong></p>
<p>By using this website, you acknowledge your assent to the following conditions of use without limitation or qualification. Please read these conditions carefully before using this website. These conditions may be revised at any time by updating this posting. You are bound by any such revisions and should therefore periodically visit this page to review the then current conditions to which you are bound.<br />
</p>


        </div>

        <div class="clear"></div>

    </div>

</section>

<!-- end of middle -->

<?php include('inc_footer.php'); ?>