<?php $page = "our_customers"; ?>

<?php include('inc_header.php'); ?>

<!-- middle -->

<section>

    <div class="wrapper">

        <div id="banner-content"><img src="images/slider/banner-ourcust.jpg" alt="Our Customers"></div>

        <nav class="share">

            <div class="left">Share:

                <a href="#"><img src="images/material/nav-tw.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-fb.png" alt=""></a>

                <a href="#"><img src="images/material/nav-mail.png" alt=""></a>

            </div>

            <div class="right">

                <a href="#"><img src="images/material/nav-zoomin.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-zoomout.png" alt=""></a>

                <a href="#"><img src="images/material/nav-print.png" alt=""></a>

            </div>

        </nav>

        <aside>

            <ul class="submenu">
                <li><a href="#" class="parent">Process Claim</a>
                	<ul>
                        <li><a href="process_claim_1.php">Pengajuan Proses Klaim </a></li>
                        <li><a href="process_claim_2.php">Pengajuan Pembayaran Klaim Meninggal Dunia </a></li>
                        <li><a href="process_claim_3.php">Pengajuan Klaim (selain klaim meninggal dunia) </a></li>
                        
                    </ul>
                </li>
                <li class="expand">  
                    <a href="#" class="parent">Payment Methods</a>
                    <ul>
                        <li><a href="payment_method_1.php">Bank Transfer</a></li>
                        <li><a href="payment_method_6.php" class="active">ATM</a></li>
                        <li><a href="payment_method_2.php">Internet Banking</a></li>
                        <li><a href="payment_method_3.php">Mobile Banking</a></li>
                        <!--<li><a href="payment_method_4.php">SMS Banking</a></li>-->
                        <li><a href="payment_method_5.php">Auto Debet</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="parent">Panduan Layanan</a>
                    <ul>
                        <li><a href="panduan_layanan_1.php">Pembatalan Pembayaran Premi Melalui Pendebetan Rekening/Kartu Kredit</a></li>
                        <li><a href="panduan_layanan_2.php">Penarikan Nilai Tunai/Dana Investasi</a></li>
                        <li><a href="panduan_layanan_3.php">Pemulihan Polis</a></li>
                        <li><a href="panduan_layanan_4.php">Pengajuan Transaksi Unit Link</a></li>
                        <li><a href="panduan_layanan_5.php">Pengajuan Pinjaman Polis</a></li>
                        <li><a href="panduan_layanan_6.php">Pengajuan Perubahan Polis</a></li>
                        <li><a href="panduan_layanan_7.php">Pengajuan Duplikat Polis dan Kartu Kesehatan</a></li>
                        <li><a href="panduan_layanan_8.php">Penarikan Manfaat Tunai dan Deviden</a></li>
                        <li><a href="panduan_layanan_9.php">Pengajuan Bankers Clause / Klausula Ban</a></li>
                    </ul>
                </li>
                <li><a href="hospital_list.php">Hospital List</a></li>
            </ul>

            <div class="side_link ">

                <div class="label">sequis link</div>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid1.png" alt="my sequis" /></span>

                    <span class="text">

                        <h6>my Sequis</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid2.png" alt="Sequisfriend"/></span>

                    <span class="text">

                        <h6>Sequisfriend</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid3.png" alt="Calculator"/> </span>

                    <span class="text">

                        <h6>Calculator</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

            </div>

            <address>

                <div class="label">get in touch</div>

                <div>

                    <img src="images/material/icon-pointer.png" alt="">

                    <h6>Sequis Group</h6>

                    <p>Sequis Center Lt. 5<br />

                        Jl. Jend. Sudirman No. 71<br />

                        Jakarta 12190, Indonesia<br />

                        T. +6221 5226 677<br />

                        F. +6221 5205 837

                    </p>

                </div>

                <a href="#">Get Direction</a> <a href="#">Send Message</a>

            </address>

        </aside>

        <div id="content">

            <nav class="breadcumb"><a href="#">Home</a> / <a href="#">Our Customers </a> / <a href="#">Payment Methods</a> / <a href="#">ATM</a></nav>

            <h2>ATM</h2>
			
            <div class="accordion">
                <div class="head">
                    Bank CIMB Niaga
                </div>
                <div class="content">                    
                <p>Masukkan 10 digit Nomor Polis.<br>
                Contoh: 2008123456-N<br>
                masukkan 2008123456</p>   
            
            	<table>
                	<tr>
                    	<th colspan="3"><h5>Pembayaran melalui ATM CIMB Niaga</h5></th>
                    
                    </tr>
                    <tr>
                    	<td><strong>Masukkan Kartu ATM CIMB Niaga Anda di mesin ATM CIMB Niaga</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-cimb-niaga/1.jpg" width="250"></td>
                    </tr>
                    <tr>
                    	<td><strong>Ketik PIN ATM CIMB Niaga Anda</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-cimb-niaga/2.jpg" width="250"></td>
                    </tr>
                    <tr>
                    	<td><strong>Setelah masuk ke rekening CIMB Niaga, klik opsi LANJUTKAN</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-cimb-niaga/3.jpg" width="250"></td>
                    </tr>
                    <tr>
                    	<td><strong>Kemudian pilih opsi PILIHAN TRANSAKSI</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-cimb-niaga/4.jpg" width="250"></td>
                    </tr>
                    <tr>
                    	<td><strong>Lalu klik opsi PEMBAYARAN</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-cimb-niaga/5.jpg" width="600"></td>
                    </tr>
                    <tr>
                    	<td><strong>Selanjutnya klik opsi LANJUT</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-cimb-niaga/6.jpg" width="600"></td>
                    </tr>
                    <tr>
                    	<td><strong>Pilih opsi ASURANSI</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-cimb-niaga/7.jpg" width="600"></td>
                    </tr>
                    <tr>
                    	<td><strong>Ketik Kode Pembayaran untuk Sequislife yakni 403. Periksa sebelum memilih opsi BENAR.  </strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-cimb-niaga/8.jpg" width="250"></td>
                    </tr>
                    <tr>
                    	<td><strong>Jika tidak ingat Kode Pembayaran untuk Sequislife, pilih opsi INFORMASI PRODUK</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-cimb-niaga/9.jpg" width="250"></td>
                    </tr>
                    <tr>
                    	<td><strong>Temukan Kode Pembayaran untuk Sequislife dan klik opsi KEMBALI untuk kembali balik pada layar Kode Pembayaran kemudian masukkan kode pembayaran Sequislife.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-cimb-niaga/10.jpg" width="250"></td>
                    </tr>
                    <tr>
                    	<td><strong>Setelah itu ketik Nomor Premi Asuransi Sequislife Anda. Periksa sebelum memilih opsi BENAR.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-cimb-niaga/11.jpg" width="250"></td>
                    </tr>
                    <tr>
                    	<td><strong>Isi kolom Nominal Pembayaran sesuai dengan premi Sequislife Anda. Periksa sebelum memilih opsi BENAR.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-cimb-niaga/12.jpg" width="250"></td>
                    </tr>
                    <tr>
                    	<td><strong>Lalu pilih rekening sumber dana untuk pembayaran tagihan.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-cimb-niaga/13.jpg" width="250"></td>
                    </tr>
                    <tr>
                    	<td><strong>Layar akan memperlihatkan informasi transaksi. Periksa sebelum memilih opsi PROSES</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-cimb-niaga/14.jpg" width="250"></td>
                    </tr>
                    <tr>
                    	<td><strong>Transaksi berhasil</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-cimb-niaga/15.jpg" width="250"></td>
                    </tr>
                    <tr>
                    	<td><strong>Pastikan Anda mengambil kartu ATM CIMB Niaga dari mesin ATM CIMB Niaga dan mengambil struk transaksi sebagai bukti pembayaran</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-cimb-niaga/16.jpg" width="250"></td>
                    </tr>
                </table>
           <br/><br/><br/>
            <a href="" class="std_link doc">
                <span class="text">Step by Step Payment Methods</span>
                <span class="file">DOC files</span>
            </a>                    
                </div>

                <div class="head">
                    BCA
                </div>
                <div class="content">
                    <p>Masukkan 10 digit Nomor Polis.
					Contoh: 2008123456-N
					masukkan 2008123456, Nomor rekening ATM BCA 0353051986
					</p>
                    <table>
                	<tr>
                    	<th colspan="3"><h5>Pembayaran melalui ATM BCA</h5></th>                    
                    </tr>
                    <tr>
                    	<td><strong>Masukkan kartu ATM BCA Anda di mesin ATM BCA</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bca/1.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Selanjutnya ketik PIN ATM BCA Anda</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bca/2.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Pilih opsi TRANSAKSI LAIN</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bca/3.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Kemudian klik opsi PEMBAYARAN</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bca/4.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Pilih opsi LAYAR BERIKUTNYA di layar menu Jenis Pembayaran</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bca/5.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Klik opsi LAIN-LAIN pada menu Jenis Pembayaran</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bca/6.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Lalu ketik NOMOR REKENING SEQUISLIFE 0353051986. Periksa sebelum memilih opsi BENAR.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bca/7.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Ketik NOMOR POLIS Sequislife Anda. Periksa sebelum memilih opsi  BENAR.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bca/8.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Isi JUMLAH PEMBAYARAN sesuai dengan premi Sequislife Anda.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bca/9.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Layar akan memperlihatkan informasi transaksi. Periksa sebelum memilih opsi BENAR.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bca/10.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Transaksi selesai</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bca/11.jpg" width="350"></td>
                    </tr>                    
                </table>
                <br/><br/><br/>
            <a href="" class="std_link doc">
                <span class="text">Step by Step Payment Methods</span>
                <span class="file">DOC files</span>
            </a>
                </div>

				<div class="head">
                    Bank Mandiri
                </div>
                <div class="content">
                    <p>Masukkan 10 digit Nomor Polis.<br>
                    Contoh: 2008123456-N<br>
                    masukkan 2008123456 
					</p>
                    <table>
                	<tr>
                    	<th colspan="3"><h5>Pembayaran melalui ATM Mandiri</h5></th>
                    
                    </tr>
                    <tr>
                    	<td><strong>Masukkan Kartu ATM Mandiri Anda pada mesin ATM Mandiri dan pilih opsi bahasa.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-mandiri/1.jpg" width="450"></td>
                    </tr>
                    <tr>
                    	<td><strong>Ketik PIN ATM Mandiri Anda</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-mandiri/2.jpg" width="450"></td>
                    </tr>
                    <tr>
                    	<td><strong>Selanjutnya pilih opsi PEMBAYARAN/PEMBELIAN</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-mandiri/3.jpg" width="450"></td>
                    </tr>
                    <tr>
                    	<td><strong>Kemudian klik opsi ASURANSI</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-mandiri/4.jpg" width="450"></td>
                    </tr>
                    <tr>
                    	<td><strong>Ketik Kode Perusahaan Asuransi Sequislife yakni 2300<br>
                        	Jika tidak ingat Kode Perusahaan untuk Sequis Life, pilih opsi DAFTAR KODE</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-mandiri/5.jpg" width="450"></td>
                    </tr>
                    <tr>
                    	<td><strong>Temukan Kode Pembayaran untuk Sequislife dan klik opsi MASUKKAN KODE untuk kembali pada layar Kode Perusahaan Asuransi</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-mandiri/6.jpg" width="450"></td>
                    </tr>
                    <tr>
                    	<td><strong>Ketik Nomor Polis Sequislife Anda. Periksa sebelum memilih opsi BENAR</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-mandiri/7.jpg" width="450"></td>
                    </tr>
                    <tr>
                    	<td><strong>Isi kolom Nominal Pembayaran sesuai dengan premi Sequislife Anda. Periksa sebelum memilih opsi BENAR.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-mandiri/8.jpg" width="450"></td>
                    </tr>
                    <tr>
                    	<td><strong>Kemudian pilih Nomor Tagihan untuk pembayaran Sequislife</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-mandiri/9.jpg" width="450"></td>
                    </tr>
                    <tr>
                    	<td><strong>Layar akan memperlihatkan informasi transaksi. Periksa sebelum memilih opsi BENAR</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-mandiri/10.jpg" width="450"></td>
                    </tr>
                    <tr>
                    	<td><strong>Pastikan Anda mengambil kartu ATM Mandiri dari mesin ATM Mandiri dan mengambil struk transaksi sebagai bukti pembayaran</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-mandiri/11.jpg" width="450"></td>
                    </tr>
                     <tr>
                    	<td><strong>Transaksi selesai</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-mandiri/12.jpg" width="450"></td>
                    </tr>                    
                </table>
                <br/><br/><br/>
                <a href="" class="std_link doc">
                    <span class="text">Step by Step Payment Methods</span>
                    <span class="file">DOC files</span>
                </a>                    
                </div>
                
                <div class="head">
                    Bank Permata
                </div>
                <div class="content">
                    <p>Masukkan 16 digit Nomor Virtual Account.<br>
                    Contoh Nomor Polis 2008123456<br>
                    nomor virtual account adalah 8803002008123456
					</p>
                    <table>
                	<tr>
                    	<th colspan="3"><h5>Pembayaran melalui ATM Permata</h5></th>
                    
                    </tr>
                    <tr>
                    	<td><strong>Masukkan Kartu ATM Bank Permata Anda di mesin ATM Permata dan pilih opsi bahasa</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-permata/1.jpg" width="500"></td>
                    </tr>
                    <tr>
                    	<td><strong>Lalu ketik PIN ATM Bank Permata Anda dan pilih opsi JIKA SELESAI, TEKAN</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-permata/2.jpg" width="500"></td>
                    </tr>
                    <tr>
                    	<td><strong>Kemudian klik opsi TRANSAKSI LAINNYA</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-permata/3.jpg" width="500"></td>
                    </tr>
                    <tr>
                    	<td><strong>Selanjutnya pilih opsi PEMBAYARAN</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-permata/4.jpg" width="500"></td>
                    </tr>
                    <tr>
                    	<td><strong>Lalu klik opsi PEMBAYARAN LAINNYA</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-permata/5.jpg" width="500"></td>
                    </tr>
                    <tr>
                    	<td><strong>Pilih opsi VIRTUAL ACCOUNT</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-permata/6.jpg" width="500"></td>
                    </tr>
                    <tr>
                    	<td><strong>Ketik Kode Virtual Account Anda. Periksa sebelum memilih opsi BENAR</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-permata/7.jpg" width="500"></td>
                    </tr>
                    <tr>
                    	<td><strong>Masukkan jumlah pembayaran sesuai dengan  premi Sequislife Anda. Layar akan memperlihatkan informasi transaksi. Periksa sebelum memilih opsi BENAR</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-permata/8.jpg" width="500"></td>
                    </tr>
                    <tr>
                    	<td><strong>Transaksi selesai</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-permata/9.jpg" width="500"></td>
                    </tr>
                    <tr>
                    	<td><strong>Pastikan Anda mengambil kartu ATM Bank Permata dari mesin ATM Permata dan mengambil struk transaksi sebagai bukti pembayaran</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-permata/10.jpg" width="500"></td>
                    </tr>                                        
                </table>
                <br/><br/><br/>
                <a href="" class="std_link doc">
                    <span class="text">Step by Step Payment Methods</span>
                    <span class="file">DOC files</span>
                </a>                    
                </div>
                
                
                <div class="head">
                    ATM Bersama
                </div>
                <div class="content">
                    <p>Menu yang digunakan adalah menu transfer.<br>
                    Masukkan 16 digit Nomor Virtual Account.<br>
                    Contoh Nomor Polis 2008123456<br>
                    nomor virtual account adalah 8803002008123456 
					</p>
                    <table>
                	<tr>
                    	<th colspan="3"><h5>Pembayaran melalui ATM Bersama</h5></th>
                    
                    </tr>
                    <tr>
                    	<td><strong>Masukkan PIN Kartu ATM Anda ke mesin ATM Bersama</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bersama/1.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Selanjutnya, klik opsi TRANSAKSI LAINNYA.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bersama/2.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Kemudian klik opsi TRANSFER.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bersama/3.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Lalu klik opsi REKENING BANK LAIN</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bersama/4.jpg" width="350"></td>
                    </tr>
                     <tr>
                    	<td><strong>Temukan kode bank sumber dana Anda lalu klik opsi MENU TRANSFER</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bersama/5.jpg" width="350"></td>
                    </tr>
                     <tr>
                    	<td><strong>Ketik kode bank sumber dana Anda, diikuti dengan Nomor Polis Sequislife Anda. Periksa sebelum memilih opsi TEKAN BILA BENAR.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bersama/6.jpg" width="350"></td>
                    </tr>
                     <tr>
                    	<td><strong>Isi JUMLAH UANG sesuai dengan premi Sequislife Anda. Periksa sebelum memilih opsi TEKAN BILA BENAR</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bersama/7.jpg" width="350"></td>
                    </tr>
                     <tr>
                    	<td><strong>Selanjutnya klik opsi TABUNGAN</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bersama/8.jpg" width="350"></td>
                    </tr>
                     <tr>
                    	<td><strong>Layar akan memperlihatkan informasi transaksi. Periksa sebelum memilih opsi YA</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bersama/9.jpg" width="350"></td>
                    </tr>
                     <tr>
                    	<td><strong>Transaksi selesai</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bersama/10.jpg" width="350"></td>
                    </tr>
                     <tr>
                    	<td><strong>Pastikan Anda mengambil Kartu ATM sebelum meninggalkan mesin ATM Bersama</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bersama/11.jpg" width="350"></td>
                    </tr>
                     <tr>
                    	<td><strong>Pastikan juga untuk mengambil struk transaksi sebagai bukti pembayaran</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/atm-bersama/12.jpg" width="350"></td>
                    </tr>                    
                </table>
                <br/><br/><br/>
            <a href="" class="std_link doc">
                <span class="text">Step by Step Payment Methods</span>
                <span class="file">DOC files</span>
            </a>              
                </div>
                
                
            </div>



        </div>

        <div class="clear"></div>

    </div>

</section>

<!-- end of middle -->

<?php include('inc_footer.php'); ?>