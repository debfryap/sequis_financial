<?php $page = "our_customers"; ?>
<?php include('inc_header.php'); ?>
<!-- middle -->
<section>
    <div class="wrapper">
        <div id="banner-content"><img src="images/slider/banner-ourcust.jpg" alt="Our Customers"></div>
        <nav class="share">
            <div class="left">Share:
                <a href="#"><img src="images/material/nav-tw.png" alt=""></a>
                <a href="#" class="center"><img src="images/material/nav-fb.png" alt=""></a>
                <a href="#"><img src="images/material/nav-mail.png" alt=""></a>
            </div>
            <div class="right">
                <a href="#"><img src="images/material/nav-zoomin.png" alt=""></a>
                <a href="#" class="center"><img src="images/material/nav-zoomout.png" alt=""></a>
                <a href="#"><img src="images/material/nav-print.png" alt=""></a>
            </div>
        </nav>
        <aside>
            <ul class="submenu">
                <li><a href="process_claim.php" class="active">Process Claim</a></li>
                <li>
                    <a href="#">Payment Methods</a>
                    <ul>
                        <li><a href="payment_method_1.php">Bank Transfer</a></li>
                        <li><a href="payment_method_2.php">Internet Banking</a></li>
                        <li><a href="payment_method_3.php">Mobile Banking</a></li>
                        <li><a href="payment_method_4.php">SMS Banking</a></li>
                        <li><a href="payment_method_5.php">Auto Debet</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Panduan Layanan</a>
                    <ul>
                        <li><a href="panduan_layanan_1.php">Pembatalan Pembayaran Premi Melalui Pendebetan Rekening/Kartu Kredit</a></li>
                        <li><a href="panduan_layanan_2.php">Penarikan Nilai Tunai/Dana Investasi</a></li>
                        <li><a href="panduan_layanan_3.php">Pemulihan Polis</a></li>
                        <li><a href="panduan_layanan_4.php">Pengajuan Transaksi Unit Link</a></li>
                        <li><a href="panduan_layanan_5.php">Pengajuan Pinjaman Polis</a></li>
                        <li><a href="panduan_layanan_6.php">Pengajuan Perubahan Polis</a></li>
                        <li><a href="panduan_layanan_7.php">Pengajuan Duplikat Polis dan Kartu Kesehatan</a></li>
                        <li><a href="panduan_layanan_8.php">Penarikan Manfaat Tunai dan Deviden</a></li>
                        <li><a href="panduan_layanan_9.php">Pengajuan Bankers Clause / Klausula Ban</a></li>
                    </ul>
                </li>
                <li><a href="hospital_list.php">Hospital List</a></li>
            </ul>
            <div class="side_link ">
                <div class="label">sequis link</div>
                <a href="">
                    <span class="ico"><img src="images/material/sq_link_mid1.png" alt="my sequis" /></span>
                    <span class="text">
                        <h6>my Sequis</h6>
                        <p>This is Photoshop's version  of Lorem Ipsum. </p>
                    </span>
                </a>
                <a href="">
                    <span class="ico"><img src="images/material/sq_link_mid2.png" alt="Sequisfriend"/></span>
                    <span class="text">
                        <h6>Sequisfriend</h6>
                        <p>This is Photoshop's version  of Lorem Ipsum. </p>
                    </span>
                </a>
                <a href="">
                    <span class="ico"><img src="images/material/sq_link_mid3.png" alt="Calculator"/> </span>
                    <span class="text">
                        <h6>Calculator</h6>
                        <p>This is Photoshop's version  of Lorem Ipsum. </p>
                    </span>
                </a>
            </div>
            <address>
                <div class="label">get in touch</div>
                <div>
                    <img src="images/material/icon-pointer.png" alt="">
                    <h6>Sequis Group</h6>
                    <p>Sequis Center Lt. 5<br />
                        Jl. Jend. Sudirman No. 71<br />
                        Jakarta 12190, Indonesia<br />
                        T. +6221 5226 677<br />
                        F. +6221 5205 837
                    </p>
                </div>
                <a href="#">Get Direction</a> <a href="#">Send Message</a>
            </address>
        </aside>
        <div id="content">
            <nav class="breadcumb"><a href="#">Home</a> / <a href="#">Our Customers </a> / <a href="#">Process Claim</a></nav>
            <h2>Process Claim</h2>

            <h6>Pendaftaran</h6>
            <ul class="ul_style">
                <li>Peserta menunjukan kartu peserta dan kartu indentitas ke petugas pendaftaran</li>
                <li>Petugas pendaftaran akan menggesek  kartu keterminal EDC</li>
                <li>Mesin EDC akan mencetak Letter of Authorization (LoA)</li>
                <li>Ditolak/ Disetujui</li>
                <li>Disetujui dan manfaat tersedia</li>
                <li>Penilaian claim oleh Admedika</li>
                <li>Surat konfirmasi dengan detail limit jaminan dan kelayakan peserta dikirimkan kerumah sakit</li>
            </ul>
            <br/>
            <br/>

            <h6>Pelayanan</h6>
            <ul class="ul_style">
                <li>Peserta mendapatkan pelayanan kesehatan</li>
                <li>Case monitoring berjalan</li>
            </ul>
            <br/>
            <br/>

            <h6>Pendaftaran Keluar</h6>
            <ul class="ul_style">
                <li>Provider akan mengirimkan tagihan akhir dan ringkasan medis ke Admedika melalui Fax</li>
                <li>Admedika  melakukan pengecekan kelayakan tagihan dan ringkasan medis</li>
                <li>Surat pernyataan Claim disetujui dan akan dikeluarkan dengan detail claim yang dapat dibayar oleh peserta atau asuransi</li>
                <li>Peserta boleh pulang</li>
            </ul>
            <br/>
            <br/>

            <a href="" class="std_link">Flow Process Cashless</a>

            <div class="contact_claim afterclear">
                <h6>Untuk pertanyaan lebih lanjut, dapat kontak Claim Helpdesk</h6>
                <div class="box_cclaim phone left" >
                    <a href="tel:02129942929">
                        <span>CONTACT</span>
                        <h5>(021) 2994 2929</h5>
                    </a>
                </div>
                <div class="box_cclaim faxmile left">
                    <a href="#">
                        <span>Faxmile</span>
                        <h5>(022) 521 3579</h5>
                    </a>
                </div>
                <div class="email left">
                    <a href="mailto:claim.helpdesk@sequislife.com">
                        Claim.Helpdesk@Sequislife.com
                    </a>
                </div>
            </div>
            <div class="download_file">
                <h5>Health</h5>
                <div class="row">
                    <a href="" class="doc">
                        <span class="text">Documentation Requirement</span>
                        <span class="file">DOC files</span>
                    </a>
                </div>
                <div class="row">
                    <a href="" class="doc">
                        <span class="text">New Claim Registration Form</span>
                        <span class="file">DOC files</span>
                    </a>
                </div>
                <div class="row">
                    <a href="" class="doc">
                        <span class="text">SKD Klaim Kesehatan SQF</span>
                        <span class="file">DOC files</span>
                    </a>
                </div>
                <div class="row">
                    <a href="" class="doc">
                        <span class="text">Surat Pernyataan Nasabah</span>
                        <span class="file">DOC files</span>
                    </a>
                </div>
            </div>

            <div class="download_file">
                <h5>Major (Non-Health)</h5>
                <div class="row">
                    <a href="" class="pdf">
                        <span class="text">Cacat Total dan Tetap</span>
                        <span class="file">PDF files</span>
                    </a>
                </div>
                <div class="row">
                    <a href="" class="pdf">
                        <span class="text">Death Claim untuk Pemegang Po</span>
                        <span class="file">PDF files</span>
                    </a>
                </div>
                <div class="row">
                    <a href="" class="pdf">
                        <span class="text">Death Claim untuk Tertanggung</span>
                        <span class="file">PDF files</span>
                    </a>
                </div>
                <div class="row">
                    <a href="" class="pdf">
                        <span class="text">Dread Diseases New</span>
                        <span class="file">PDF files</span>
                    </a>
                </div>
                <div class="row">
                    <a href="" class="pdf">
                        <span class="text">Waiver Premium</span>
                        <span class="file">PDF files</span>
                    </a>
                </div>
            </div>

            <div class="download_file">
                <h5>Maternity (only for corporate)</h5>
                <div class="row">
                    <a href="" class="doc">
                        <span class="text">Dokumen pengajuan klaim</span>
                        <span class="file">DOC files</span>
                    </a>
                </div>                
            </div>

        </div>
        <div class="clear"></div>
    </div>
</section>
<!-- end of middle -->
<?php include('inc_footer.php'); ?>