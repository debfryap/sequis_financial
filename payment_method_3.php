<?php $page = "our_customers"; ?>

<?php include('inc_header.php'); ?>

<!-- middle -->

<section>

    <div class="wrapper">

        <div id="banner-content"><img src="images/slider/banner-ourcust.jpg" alt="Our Customers"></div>

        <nav class="share">

            <div class="left">Share:

                <a href="#"><img src="images/material/nav-tw.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-fb.png" alt=""></a>

                <a href="#"><img src="images/material/nav-mail.png" alt=""></a>

            </div>

            <div class="right">

                <a href="#"><img src="images/material/nav-zoomin.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-zoomout.png" alt=""></a>

                <a href="#"><img src="images/material/nav-print.png" alt=""></a>

            </div>

        </nav>

        <aside>

            <ul class="submenu">
                <li><a href="#" class="parent">Process Claim</a>
                	<ul>
                        <li><a href="process_claim_1.php">Pengajuan Proses Klaim </a></li>
                        <li><a href="process_claim_2.php">Pengajuan Pembayaran Klaim Meninggal Dunia </a></li>
                        <li><a href="process_claim_3.php">Pengajuan Klaim (selain klaim meninggal dunia) </a></li>
                        
                    </ul>
                </li>
                <li class="expand">  
                    <a href="#" class="parent">Payment Methods</a>
                    <ul>
                        <li><a href="payment_method_1.php">Bank Transfer</a></li>
                        <li><a href="payment_method_6.php">ATM</a></li>
                        <li><a href="payment_method_2.php">Internet Banking</a></li>
                        <li><a href="payment_method_3.php" class="active">Mobile Banking</a></li>
                        <!--<li><a href="payment_method_4.php">SMS Banking</a></li>-->
                        <li><a href="payment_method_5.php">Auto Debet</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="parent">Panduan Layanan</a>
                    <ul>
                        <li><a href="panduan_layanan_1.php">Pembatalan Pembayaran Premi Melalui Pendebetan Rekening/Kartu Kredit</a></li>
                        <li><a href="panduan_layanan_2.php">Penarikan Nilai Tunai/Dana Investasi</a></li>
                        <li><a href="panduan_layanan_3.php">Pemulihan Polis</a></li>
                        <li><a href="panduan_layanan_4.php">Pengajuan Transaksi Unit Link</a></li>
                        <li><a href="panduan_layanan_5.php">Pengajuan Pinjaman Polis</a></li>
                        <li><a href="panduan_layanan_6.php">Pengajuan Perubahan Polis</a></li>
                        <li><a href="panduan_layanan_7.php">Pengajuan Duplikat Polis dan Kartu Kesehatan</a></li>
                        <li><a href="panduan_layanan_8.php">Penarikan Manfaat Tunai dan Deviden</a></li>
                        <li><a href="panduan_layanan_9.php">Pengajuan Bankers Clause / Klausula Ban</a></li>
                    </ul>
                </li>
                <li><a href="hospital_list.php">Hospital List</a></li>
            </ul>

            <div class="side_link ">

                <div class="label">sequis link</div>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid1.png" alt="my sequis" /></span>

                    <span class="text">

                        <h6>my Sequis</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid2.png" alt="Sequisfriend"/></span>

                    <span class="text">

                        <h6>Sequisfriend</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid3.png" alt="Calculator"/> </span>

                    <span class="text">

                        <h6>Calculator</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

            </div>

            <address>

                <div class="label">get in touch</div>

                <div>

                    <img src="images/material/icon-pointer.png" alt="">

                    <h6>Sequis Group</h6>

                    <p>Sequis Center Lt. 5<br />

                        Jl. Jend. Sudirman No. 71<br />

                        Jakarta 12190, Indonesia<br />

                        T. +6221 5226 677<br />

                        F. +6221 5205 837

                    </p>

                </div>

                <a href="#">Get Direction</a> <a href="#">Send Message</a>

            </address>

        </aside>

        <div id="content">

            <nav class="breadcumb"><a href="#">Home</a> / <a href="#">Our Customers </a> / <a href="#">Payment Methods</a> / <a href="#">Mobile Banking</a></nav>

            <h2>Mobile Banking</h2>
			
            <div class="accordion">
                <div class="head">
                    M-BCA
                </div>
                <div class="content">
                    <table>                
                    <tr>
                    	<td><strong>Pilih menu m.life m-BCA di telepon genggam Anda dan tekan OK/YES<br>
									Kemudian klik opsi m-Payment dan tekan OK/YES
							</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/m-bca/1.png" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Selanjutnya pilih menu Lainnya dan tekan OK/YES</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/m-bca/2.png" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Ketik Kode Perusahaan dan tekan OK/YES</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/m-bca/3.png" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Ketik Nomor Pelanggan Anda dan tekan OK/YES</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/m-bca/4.png" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Isi Jumlah Pembayaran sesuai dengan premi Sequislife Anda dan tekan OK/YES</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/m-bca/5.png" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Ketik PIN m-BCA Anda dan tekan OK/YES</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/m-bca/6.png" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Pilih Rekening Sumber Dana yang akan didebet dan tekan OK/YES
(Layar ini hanya tampil jika Anda memiliki lebih dari satu rekening BCA)
</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/m-bca/7.png" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Pastikan data transaksi pembayaran telah benar sebelum menekan OK/YES</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/m-bca/8.png" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Ketik PIN m-BCA dan tekan OK/YES</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/m-bca/9.png" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Informasi transaksi pembayaran akan ditampilkan</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/m-bca/10.png" width="350"></td>
                    </tr>
                                                                              
                </table>

                </div>

                <div class="head">
                    SMS Banking Mandiri
                </div>
                <div class="content">
                  <table>                
                    <tr>
                    	<td><strong>Pilih Menu SMS / Messages pada telepon genggam Anda.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/sms-mandiri/1.jpg" width="350"></td>
                    </tr
                    ><tr>
                    	<td><strong>Ketik BYR SEQ <nomor polis> <PIN> lalu kirim ke 3355.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/sms-mandiri/2.png" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Anda akan menerima SMS balasan sebagai konfirmasi Anda telah membayar Premi.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/sms-mandiri/3.png" width="350"></td>
                    </tr>>                                                                                                 
                </table>  
                    
                </div>
                
                <div class="head">
                    Permata Mobile
                </div>
                <div class="content">
                    <table>                
                    <tr>
                    	<td><strong>Pilih menu Virtual Account di Mobile Banking Permata.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/permata-mobile/1.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Ketik Nomor Tagihan kemudian isi kolom Jumlah Pembayaran sesuai dengan premi Sequislife Anda.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/permata-mobile/2.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Pastikan data transaksi pembayaran telah benar sebelum menekan Send</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/permata-mobile/3.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Konfirmasi dari bank akan ditampilkan pada layar.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/permata-mobile/4.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Kirim data sesuai konfirmasi dari bank.<br>
                        			Contoh: ketik "TIN <spasi> digit ke-6 dan 2 dari TIN Anda, kirim ke 3399 dan tekan Send.
                        	</strong>
                        </td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/permata-mobile/5.jpg" width="350"></td>
                    </tr>
                    <tr>
                    	<td><strong>Konfirmasi pembayaran berhasil akan ditampilkan pada layar.</strong></td>
                        <td>&nbsp;</td>
                        <td><img src="images/content/permata-mobile/6.jpg" width="350"></td>
                    </tr>                                                                                                                      
                </table>
                    
                </div>
                
              


            </div>



        </div>

        <div class="clear"></div>

    </div>

</section>

<!-- end of middle -->

<?php include('inc_footer.php'); ?>