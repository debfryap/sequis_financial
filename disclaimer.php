<?php $page = "our_product"; ?>

<?php include('inc_header.php'); ?>

<!-- middle -->

<section>

    <div class="wrapper">

        <div id="banner-content"><img src="images/slider/banner-desclaimer.jpg" alt="Disclaimer"></div>

        <nav class="share">

            <div class="left">Share:

                <a href="#"><img src="images/material/nav-tw.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-fb.png" alt=""></a>

                <a href="#"><img src="images/material/nav-mail.png" alt=""></a>

            </div>

            <div class="right">

                <a href="#"><img src="images/material/nav-zoomin.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-zoomout.png" alt=""></a>

                <a href="#"><img src="images/material/nav-print.png" alt=""></a>

            </div>

        </nav>

        <aside>

             <div class="side_link ">

                <div class="label">sequis link</div>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid1.png" alt="my sequis" /></span>

                    <span class="text">

                        <h6>my Sequis</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid2.png" alt="Sequisfriend"/></span>

                    <span class="text">

                        <h6>Sequisfriend</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid3.png" alt="Calculator"/> </span>

                    <span class="text">

                        <h6>Calculator</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

            </div>

            <address>

                <div class="label">get in touch</div>

                <div>

                    <img src="images/material/icon-pointer.png" alt="">

                    <h6>Sequis Group</h6>

                    <p>Sequis Center Lt. 5<br />

                        Jl. Jend. Sudirman No. 71<br />

                        Jakarta 12190, Indonesia<br />

                        T. +6221 5226 677<br />

                        F. +6221 5205 837

                    </p>

                </div>

                <a href="#">Get Direction</a> <a href="#">Send Message</a>

            </address>

        </aside>

        <div id="content">

            <nav class="breadcumb"><a href="#">Home</a> / <a href="#">Our Products </a> / <a href="#">Life Protection </a></nav>

            <h2>Disclaimer</h2>

            <p><strong>DISCLAIMER</strong><br />
To the fullest extent permissible pursuant to applicable law: (1) the materials on this website are provided &quot;as is&quot; and without warranties of any kind either expressed or implied  disclaim all warranties, expressed or implied, including, but not limited to, implied warranties of merchantability and fitness for a particular purpose; (2) Sequislife does not warrant that the website or the functions contained in the materials will be uninterrupted or error-free, that defects will be corrected, or that this website or the server that makes it available are free of viruses or other harmful components; and (3) Sequislife does not warrant or make any representations regarding the use or the results of the use of the materials on this website in terms of their correctness, accuracy, reliability, or otherwise. The information and descriptions contained herein are not necessarily intended to be complete descriptions of all terms, exclusions and conditions applicable to the products and services, but are provided solely for general informational purposes. For complete details please refer to the actual policy or the relevant product or services agreement.</p>
<p>This website may be linked to other websites which are not maintained by Sequislife. Sequislife is not responsible for the availability, content or accuracy of any external websites and does not make any warranty, express or implied, with respect to the use of any external links. The inclusion of any link to such websites does not imply approval of or endorsement by Sequislife of the websites or the content thereof. You acknowledge and agree that you access such websites at your own risk.</p>
<p><strong>RESTRICTIONS ON USE OF MATERIALS</strong><br />
This website is owned and operated by Sequislife. Except as otherwise expressly permitted by Sequislife , no materials from this website or any website owned, operated, licensed or controlled by Sequislife  may be copied, reproduced, republished, modified, stored in a retrieval system, used for creating derivative works, uploaded, posted, transmitted (by any form or by any means), distributed or used in any other way for public or commercial purposes. You may download material displayed on this website for your use only, provided that you also retain all copyright and other proprietary notices contained on the materials. You may not distribute, modify, transmit, reuse, repost, or use the content of this website, for public or commercial purposes, including the text, images, audio, and video, without Sequislife's written permission.</p>
<p>Sequislife neither warrants nor represents that your use of materials displayed on this website will not infringe rights of third parties.</p>
<p><strong>INTELLECTUAL PROPERTY</strong><br />
  All trademarks, service marks, trade names, logos, and icons are proprietary to Sequislife. Other than as expressly set out in these Conditions and except as permitted by applicable law, nothing contained on the website should be construed as granting, by implication, estoppel, or otherwise, any license or right to use any of Sequislife’s intellectual property including, but not limited to, any trademark or logo displayed on this website without the written permission of Sequislife. Your use of the trademarks and logos displayed on this website, or any other content on this website, except as provided herein, is strictly prohibited.<br />
</p>
<p><strong>SOFTWARE LICENSES</strong><br />
To the extent that any software is made available or provided to you on this website, Sequislife grants you a non-exclusive, limited and personal license to download and use the software for your own personal, non-commercial use only. No other rights to the software are granted. All rights including intellectual property rights in the software remain the property of Sequislife or the relevant licensors or owners of the software and, except as permitted by law, you may not reverse engineer, decompile, modify or tamper with the software.</p>
<p><strong>GOVERNING LAW AND JURISIDICTION</strong><br />
  These Conditions will be governed by the laws of the Republic of Indonesia and the parties are agree to submit to the exclusive jurisdiction of the courts of Jakarta Selatan.<br />
</p>


        </div>

        <div class="clear"></div>

    </div>

</section>

<!-- end of middle -->

<?php include('inc_footer.php'); ?>