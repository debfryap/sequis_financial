<!DOCTYPE html>

<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->

<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->

<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->

<!--[if gt IE 8]><!-->

<html class="no-js">

    <!--<![endif]-->

    <head>

        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title>WEBARQ - Static Website</title>

        <meta name="description" content="">

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />



        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="icon" href="favicon.ico">

        <!--Style-->

        <link rel="stylesheet" href="css/reset.css">

        <link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />

        <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" media="screen" />

        <link href="css/timeline.css" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="css/style.css">



        <!--js-->

        <script src="js/vendor/jquery-1.9.1.min.js"></script>

        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>

        <script src="js/plugins.js"></script>

        <script src="js/vendor/modernizr-2.6.2.min.js"></script>

        <script type="text/javascript" src="js/jquery.fancybox.js"></script>

        <script src="js/jquery.timelinr-0.9.54.js"></script>

        <script src="js/jquery_function.js"></script>

    </head>

    <body>

        <!--[if lt IE 7]>

                    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>

                <![endif]--> 



        <!-- Add your site or application content here --> 



        <!-- header -->

        <header>

            <div class="wrapper">

                <div class="top">

                    <div class="left">

                        <ul>

                            <li><a href="http://client.webarq.com/sequis/">Home</a></li>

                            <li><a href="http://client.webarq.com/sequis_individu/">Individual</a></li>

                            <li><a href="index.php" class="active">Corporate</a></li>

                            <li><a href="#">Asset Management</a></li>

                            <li><a href="#">E-Insurance</a></li>

                        </ul>

                    </div>

                    <div class="right"><span class="livechat"><a href="#">Live Chat</a></span>&nbsp;&nbsp;<span class="career"><a href="career.php">SQF Portal</a></span>

                        <div class="language">

                            <select name="" class="lang">

                                <option>ENG</option>

                                <option>IDN</option>

                            </select>

                        </div>

                        <form action="" method="get">

                            <input name="" type="text">

                        </form>

                    </div>

                </div>

                <div class="bottom afterclear">

                    <div class="logo"><a href="index.php"><img src="images/material/logo.png" alt=""></a></div>

                    <ul>

                        <li>

                            <a href="about_us.php" <?php echo ($page == "about" ? 'class="active"' : ""); ?>>About Us</a>

                        </li>

                        <li class="have_drop"><a href="product.php" <?php echo ($page == "our_product" ? 'class="active"' : ""); ?>>Our Products</a>

                            <div class="area_hover">

                                <div class="megabox">

                                    <div class="column">

                                        <ul>

                                            <li><a href="product-01.php" class="active">Group Life</a></li>

                                            <li><a href="product-02.php">Group Health</a></li>

                                            <li><a href="product-03.php">Group Saving</a></li>

                                        </ul>

                                    </div>

                                    

                                </div>

                            </div>

                        </li>

                        <li class="have_drop"><a href="process_claim_1.php" <?php echo ($page == "our_customers" ? 'class="active"' : ""); ?>>Our Customers</a>

                            <div class="area_hover">

                                <div class="megabox">

                                    <div class="column">

                                        <ul>
                                            <li><a href="process_claim_1.php">Process Claim</a></li>
                                            <li><a href="payment_method_1.php">Payment Methods</a></li>
                                            <li><a href="panduan_layanan_1.php">Panduan Layanan</a></li>
                                            <li><a href="hospital_list.php">Hospital List</a></li>
                                        </ul>

                                    </div>

                                    

                                </div>

                            </div>

                        </li>

                        <li>

                            <a href="contact.php" <?php echo ($page == "contact" ? 'class="active"' : ""); ?>>Contact Us</a>

                            <!--<div class="area_hover">

                                <div class="megabox">

                                    <div class="column">

                                        <ul>

                                            <li><a href="contact.php" class="active">Enquiries</a></li>

                                            <li><a href="contact-02.php">FAQ</a></li>

                                        </ul>

                                    </div>

                                    <div class="column save">

                                        <h4>Saver Plan</h4>

                                        <p>This is Photoshop's version of Lorem IpsumSit dolor</p>

                                        <a href="#">Learn More.</a>

                                    </div>

                                </div>

                            </div>-->

                        </li>

                    </ul>

                </div>

            </div>

        </header>

        <!-- end of header -->