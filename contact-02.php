<?php $page = "contact"; ?>
<?php include('inc_header.php');?>
<!-- middle -->
<section>
  <div class="wrapper">
    <div id="banner-content"><img src="images/slider/banner-contact.jpg" alt="Contact us"></div>
    <nav class="share">
      <div class="left">Share: <a href="#"><img src="images/material/nav-tw.png" alt=""></a> <a href="#" class="center"><img src="images/material/nav-fb.png" alt=""></a> <a href="#"><img src="images/material/nav-mail.png" alt=""></a></div>
      <div class="right"><a href="#"><img src="images/material/nav-zoomin.png" alt=""></a> <a href="#" class="center"><img src="images/material/nav-zoomout.png" alt=""></a> <a href="#"><img src="images/material/nav-print.png" alt=""></a></div>
    </nav>
    <aside>
      <ul class="submenu">
        <li><a href="contact.php">Enquiries</a></li>
        <li><a href="contact-02.php" class="active">FAQ</a></li>
      </ul>
      <address>
      <div class="label">get in touch</div>
      <div><img src="images/material/icon-pointer.png" alt="">
        <h6>Sequis Group</h6>
        <p>Sequis Center Lt. 5<br />
          Jl. Jend. Sudirman No. 71<br />
          Jakarta 12190, Indonesia<br />
          T. +6221 5226 677<br />
          F. +6221 5205 837</p>
      </div>
      <a href="#">Get Direction</a> <a href="#">Send Message</a>
      </address>
    </aside>
    <div id="content">
      <nav class="breadcumb"><a href="#">Home</a> / <a href="#">Contact Us</a> / <a href="#">FAQ</a></nav>
      <h2>FAQ</h2>
      <ul class="accordion">
        <li><a href="#">Sejarah berdirinya Sequis Life dan anak perusahaannya, Sequis Financial?</a>
          <div class="answer">
            <p><strong>Sequis Life</strong><br />
              1984	Awal pendirian dibawah PT. Universal  Life Indo ( ULINDO )<br />
              1992	Mengikatkan diri dalam joint venture agreement dengan New York Life <br />
              atau NYL International in Corporation dengan nama Sewu New York Life<br />
              2003	Membeli semua saham NYL dan mengubah nama menjadi Sequis Life</p>
            <p><strong>Sequis Financial</strong><br />
              2005	Sequis Life mengakuisisi Met Life Indonesia dan mengubah nama Met Life Indonesia menjadi Sequis Financial</p>
          </div>
        </li>
        <li><a href="#">Siapakah Sequis Life ?</a>
          <div class="answer">
            <p><strong>Sequis Life</strong><br />
              1984	Awal pendirian dibawah PT. Universal  Life Indo ( ULINDO )<br />
              1992	Mengikatkan diri dalam joint venture agreement dengan New York Life <br />
              atau NYL International in Corporation dengan nama Sewu New York Life<br />
              2003	Membeli semua saham NYL dan mengubah nama menjadi Sequis Life</p>
            <p><strong>Sequis Financial</strong><br />
              2005	Sequis Life mengakuisisi Met Life Indonesia dan mengubah nama Met Life Indonesia menjadi Sequis Financial</p>
          </div>
        </li>
        <li><a href="#">Dimanakah saya dapat menghubungi Sequis Life dan anak perusahaannya, Sequis Financial ?</a>
          <div class="answer">
            <p><strong>Sequis Life</strong><br />
              1984	Awal pendirian dibawah PT. Universal  Life Indo ( ULINDO )<br />
              1992	Mengikatkan diri dalam joint venture agreement dengan New York Life <br />
              atau NYL International in Corporation dengan nama Sewu New York Life<br />
              2003	Membeli semua saham NYL dan mengubah nama menjadi Sequis Life</p>
            <p><strong>Sequis Financial</strong><br />
              2005	Sequis Life mengakuisisi Met Life Indonesia dan mengubah nama Met Life Indonesia menjadi Sequis Financial</p>
          </div>
        </li>
        <li><a href="#">Mengapa Nippon Life Insurance Company  melakukan kemitraan strategis di PT. Asuransi Jiwa Sequis Life?</a>
          <div class="answer">
            <p><strong>Sequis Life</strong><br />
              1984	Awal pendirian dibawah PT. Universal  Life Indo ( ULINDO )<br />
              1992	Mengikatkan diri dalam joint venture agreement dengan New York Life <br />
              atau NYL International in Corporation dengan nama Sewu New York Life<br />
              2003	Membeli semua saham NYL dan mengubah nama menjadi Sequis Life</p>
            <p><strong>Sequis Financial</strong><br />
              2005	Sequis Life mengakuisisi Met Life Indonesia dan mengubah nama Met Life Indonesia menjadi Sequis Financial</p>
          </div>
        </li>
        <li><a href="#">Apakah keuntungan memiliki asuransi jiwa ?</a>
          <div class="answer">
            <p><strong>Sequis Life</strong><br />
              1984	Awal pendirian dibawah PT. Universal  Life Indo ( ULINDO )<br />
              1992	Mengikatkan diri dalam joint venture agreement dengan New York Life <br />
              atau NYL International in Corporation dengan nama Sewu New York Life<br />
              2003	Membeli semua saham NYL dan mengubah nama menjadi Sequis Life</p>
            <p><strong>Sequis Financial</strong><br />
              2005	Sequis Life mengakuisisi Met Life Indonesia dan mengubah nama Met Life Indonesia menjadi Sequis Financial</p>
          </div>
        </li>
        <li><a href="#">Apa saja produk dan layanan yang ada di Sequis life ?</a>
          <div class="answer">
            <p><strong>Sequis Life</strong><br />
              1984	Awal pendirian dibawah PT. Universal  Life Indo ( ULINDO )<br />
              1992	Mengikatkan diri dalam joint venture agreement dengan New York Life <br />
              atau NYL International in Corporation dengan nama Sewu New York Life<br />
              2003	Membeli semua saham NYL dan mengubah nama menjadi Sequis Life</p>
            <p><strong>Sequis Financial</strong><br />
              2005	Sequis Life mengakuisisi Met Life Indonesia dan mengubah nama Met Life Indonesia menjadi Sequis Financial</p>
          </div>
        </li>
        <li><a href="#">Bagaimana cara saya melakukan pembayaran premi ?</a>
          <div class="answer">
            <p><strong>Sequis Life</strong><br />
              1984	Awal pendirian dibawah PT. Universal  Life Indo ( ULINDO )<br />
              1992	Mengikatkan diri dalam joint venture agreement dengan New York Life <br />
              atau NYL International in Corporation dengan nama Sewu New York Life<br />
              2003	Membeli semua saham NYL dan mengubah nama menjadi Sequis Life</p>
            <p><strong>Sequis Financial</strong><br />
              2005	Sequis Life mengakuisisi Met Life Indonesia dan mengubah nama Met Life Indonesia menjadi Sequis Financial</p>
          </div>
        </li>
        <li><a href="#">Apa akibat yang ditimbulkan bila saya tidak melakukan pembayaran premi   tepat waktu ?</a>
          <div class="answer">
            <p><strong>Sequis Life</strong><br />
              1984	Awal pendirian dibawah PT. Universal  Life Indo ( ULINDO )<br />
              1992	Mengikatkan diri dalam joint venture agreement dengan New York Life <br />
              atau NYL International in Corporation dengan nama Sewu New York Life<br />
              2003	Membeli semua saham NYL dan mengubah nama menjadi Sequis Life</p>
            <p><strong>Sequis Financial</strong><br />
              2005	Sequis Life mengakuisisi Met Life Indonesia dan mengubah nama Met Life Indonesia menjadi Sequis Financial</p>
          </div>
        </li>
        <li><a href="#">Bila polis yang saya miliki batal ( Lapse ), apa kerugiannya, dan bisakah diaktifkan agar pertanggungan dapat berjalan kembali?</a>
          <div class="answer">
            <p><strong>Sequis Life</strong><br />
              1984	Awal pendirian dibawah PT. Universal  Life Indo ( ULINDO )<br />
              1992	Mengikatkan diri dalam joint venture agreement dengan New York Life <br />
              atau NYL International in Corporation dengan nama Sewu New York Life<br />
              2003	Membeli semua saham NYL dan mengubah nama menjadi Sequis Life</p>
            <p><strong>Sequis Financial</strong><br />
              2005	Sequis Life mengakuisisi Met Life Indonesia dan mengubah nama Met Life Indonesia menjadi Sequis Financial</p>
          </div>
        </li>
        <li><a href="#">Kapan saya dapat mengajukan pemulihan atas polis yang batal / lapse ?</a>
          <div class="answer">
            <p><strong>Sequis Life</strong><br />
              1984	Awal pendirian dibawah PT. Universal  Life Indo ( ULINDO )<br />
              1992	Mengikatkan diri dalam joint venture agreement dengan New York Life <br />
              atau NYL International in Corporation dengan nama Sewu New York Life<br />
              2003	Membeli semua saham NYL dan mengubah nama menjadi Sequis Life</p>
            <p><strong>Sequis Financial</strong><br />
              2005	Sequis Life mengakuisisi Met Life Indonesia dan mengubah nama Met Life Indonesia menjadi Sequis Financial</p>
          </div>
        </li>
        <li><a href="#">Bagaimana saya dapat mengajukan klaim ?</a>
          <div class="answer">
            <p><strong>Sequis Life</strong><br />
              1984	Awal pendirian dibawah PT. Universal  Life Indo ( ULINDO )<br />
              1992	Mengikatkan diri dalam joint venture agreement dengan New York Life <br />
              atau NYL International in Corporation dengan nama Sewu New York Life<br />
              2003	Membeli semua saham NYL dan mengubah nama menjadi Sequis Life</p>
            <p><strong>Sequis Financial</strong><br />
              2005	Sequis Life mengakuisisi Met Life Indonesia dan mengubah nama Met Life Indonesia menjadi Sequis Financial</p>
          </div>
        </li>
        <li><a href="#">Bagaimana bila polis yang saya miliki hilang dan dapatkah saya dibuatkan  polis yang baru?</a>
          <div class="answer">
            <p><strong>Sequis Life</strong><br />
              1984	Awal pendirian dibawah PT. Universal  Life Indo ( ULINDO )<br />
              1992	Mengikatkan diri dalam joint venture agreement dengan New York Life <br />
              atau NYL International in Corporation dengan nama Sewu New York Life<br />
              2003	Membeli semua saham NYL dan mengubah nama menjadi Sequis Life</p>
            <p><strong>Sequis Financial</strong><br />
              2005	Sequis Life mengakuisisi Met Life Indonesia dan mengubah nama Met Life Indonesia menjadi Sequis Financial</p>
          </div>
        </li>
        <li><a href="#">Bagaimana cara saya menginformasikan apabila ada perubahan yang sudah tidak sesuai dengan polis atau data kontak pribadi? </a>
          <div class="answer">
            <p><strong>Sequis Life</strong><br />
              1984	Awal pendirian dibawah PT. Universal  Life Indo ( ULINDO )<br />
              1992	Mengikatkan diri dalam joint venture agreement dengan New York Life <br />
              atau NYL International in Corporation dengan nama Sewu New York Life<br />
              2003	Membeli semua saham NYL dan mengubah nama menjadi Sequis Life</p>
            <p><strong>Sequis Financial</strong><br />
              2005	Sequis Life mengakuisisi Met Life Indonesia dan mengubah nama Met Life Indonesia menjadi Sequis Financial</p>
          </div>
        </li>
      </ul>
    </div>
    <div class="clear"></div>
  </div>
</section>
<!-- end of middle -->
<?php include('inc_footer.php');?>