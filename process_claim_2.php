<?php $page = "our_customers"; ?>

<?php include('inc_header.php'); ?>

<!-- middle -->

<section>

    <div class="wrapper">

        <div id="banner-content"><img src="images/slider/banner-ourcust.jpg" alt="Our Customers"></div>

        <nav class="share">

            <div class="left">Share:

                <a href="#"><img src="images/material/nav-tw.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-fb.png" alt=""></a>

                <a href="#"><img src="images/material/nav-mail.png" alt=""></a>

            </div>

            <div class="right">

                <a href="#"><img src="images/material/nav-zoomin.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-zoomout.png" alt=""></a>

                <a href="#"><img src="images/material/nav-print.png" alt=""></a>

            </div>

        </nav>

        <aside>

            <ul class="submenu">
                <li class="expand"><a href="#" class="parent">Process Claim</a>
                	<ul>
                        <li><a href="process_claim_1.php">Proses Klaim Kesehatan </a></li>
                        <li><a href="process_claim_2.php" class="active">Pengajuan Pembayaran Klaim Meninggal Dunia </a></li>
                        <li><a href="process_claim_3.php">Pengajuan Klaim (selain klaim meninggal dunia) </a></li>
                        
                    </ul>
                </li>
                <li>  
                    <a href="#" class="parent">Payment Methods</a>
                    <ul>
                        <li><a href="payment_method_1.php">Bank Transfer</a></li>
                        <li><a href="payment_method_6.php">ATM</a></li>
                        <li><a href="payment_method_2.php">Internet Banking</a></li>
                        <li><a href="payment_method_3.php">Mobile Banking</a></li>
                        <!--<li><a href="payment_method_4.php">SMS Banking</a></li>-->
                        <li><a href="payment_method_5.php">Auto Debet</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="parent">Panduan Layanan</a>
                    <ul>
                        <li><a href="panduan_layanan_1.php">Pembatalan Pembayaran Premi Melalui Pendebetan Rekening/Kartu Kredit</a></li>
                        <li><a href="panduan_layanan_2.php">Penarikan Nilai Tunai/Dana Investasi</a></li>
                        <li><a href="panduan_layanan_3.php">Pemulihan Polis</a></li>
                        <li><a href="panduan_layanan_4.php">Pengajuan Transaksi Unit Link</a></li>
                        <li><a href="panduan_layanan_5.php">Pengajuan Pinjaman Polis</a></li>
                        <li><a href="panduan_layanan_6.php">Pengajuan Perubahan Polis</a></li>
                        <li><a href="panduan_layanan_7.php">Pengajuan Duplikat Polis dan Kartu Kesehatan</a></li>
                        <li><a href="panduan_layanan_8.php">Penarikan Manfaat Tunai dan Deviden</a></li>
                        <li><a href="panduan_layanan_9.php">Pengajuan Bankers Clause / Klausula Ban</a></li>
                    </ul>
                </li>
                <li><a href="hospital_list.php">Hospital List</a></li>
            </ul>

            <div class="side_link ">

                <div class="label">sequis link</div>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid1.png" alt="my sequis" /></span>

                    <span class="text">

                        <h6>my Sequis</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid2.png" alt="Sequisfriend"/></span>

                    <span class="text">

                        <h6>Sequisfriend</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid3.png" alt="Calculator"/> </span>

                    <span class="text">

                        <h6>Calculator</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

            </div>

            <address>

                <div class="label">get in touch</div>

                <div>

                    <img src="images/material/icon-pointer.png" alt="">

                    <h6>Sequis Group</h6>

                    <p>Sequis Center Lt. 5<br />

                        Jl. Jend. Sudirman No. 71<br />

                        Jakarta 12190, Indonesia<br />

                        T. +6221 5226 677<br />

                        F. +6221 5205 837

                    </p>

                </div>

                <a href="#">Get Direction</a> <a href="#">Send Message</a>

            </address>

        </aside>

        <div id="content">

            <nav class="breadcumb"><a href="#">Home</a> / <a href="#">Our Customers </a> / <a href="#">Process Claim</a>/ <a href="#">Pengajuan Pembayaran Klaim Meninggal Dunia</a></nav>

            <h2>Pengajuan Pembayaran Klaim Meninggal Dunia</h2>
			<div class="accordion">
                <div class="head">
                    Prosedur Pengajuan Klaim Manfaat Meninggal Dunia<br><small>(Jika Tertanggung meninggal dunia)</small>
                </div>
                <div class="content">
                <p>Proses pengajuan klaim meninggal dunia dilakukan oleh ahli waris apabila Tertanggung meninggal dunia.</p>
                <p>Prosedur pengajuan klaim:</p>
                    <ul class="ul_style">
                    	<li>Formulir Pengajuan  Klaim  (Form Sequislife)</li>
                        <li>Surat Keterangan Ahli Waris (Form Sequislife)</li>
                        <li>Surat Keterangan Dokter (Form Sequislife)</li>
                        <li>Fotokopi identitas Tertanggung, Ahli Waris atau pihak yang ditunjuk yang masih berlaku</li>
                        <li>Akta Meninggal Dunia (asli/legalisir) atau Surat Keterangan Meninggal dari Pemerintah Daerah dan dari pihak medis (asli/legalisir)</li>
                        <li>Surat Keterangan Kecelakaan dari kepolisian apabila meninggal karena kecelakaan</li>
                        <li>Surat keterangan/ dokumen lainnya yang mungkin diperlukan</li>
                    </ul>
                    
                    <div class="download_file">
                <h6>Formulir  :</h6>
                    <div class="row">
                        <a href="" class="doc">
                            <span class="text">Pengajuan Klaim</span>
                            <span class="file">DOC files</span>
                        </a>
                    </div>
                    <div class="row">
                        <a href="" class="doc">
                            <span class="text">Surat Keterangan Dokter</span>
                            <span class="file">DOC files</span>
                        </a>
                    </div>
                    <div class="row">
                        <a href="" class="doc">
                            <span class="text">Surat Keterangan Ahli Waris</span>
                            <span class="file">DOC files</span>
                        </a>
                    </div>
            	</div>             
                   
                </div>

                <div class="head">
                    Prosedur Pengajuan Klaim Manfaat Meninggal Dunia<br>
                    <small>(Jika Pemegang Polis Meninggal Dunia)</small>
                </div>
                <div class="content">
                <ul class="ul_style">
                    	<li>Formulir Pengajuan Klaim  (Form Sequislife)</li>
                        <li>Formulir Keterangan Ahli Waris (Form Sequislife)</li>
                        <li>Formulir Keterangan Dokter (Form Sequislife)</li>
                        <li>Fotokopi identitas Pemegang Polis dan Pengajuan Klaim</li>
                        <li>Akta Kematian (asli/legalisir) atau Surat Keterangan Meninggal dari Pemerintah Daerah dan dari pihak medis (asli/legalisir)</li>
                        <li>Surat Keterangan Kecelakaan dari kepolisian apabila meninggal karena kecelakaan</li>
                        <li>Surat keterangan/ dokumen lainnya yang mungkin diperlukan</li>
                    </ul>
                    
                    <div class="download_file">
                <h6>Formulir  :</h6>
                    <div class="row">
                        <a href="" class="doc">
                            <span class="text">Pengajuan Klaim</span>
                            <span class="file">DOC files</span>
                        </a>
                    </div>
                    <div class="row">
                        <a href="" class="doc">
                            <span class="text">Surat Keterangan Dokter</span>
                            <span class="file">DOC files</span>
                        </a>
                    </div>
                    <div class="row">
                        <a href="" class="doc">
                            <span class="text">Surat Keterangan Ahli Waris</span>
                            <span class="file">DOC files</span>
                        </a>
                    </div>
            	</div>
                    
                </div>
            </div>           



        </div>

        <div class="clear"></div>

    </div>

</section>

<!-- end of middle -->

<?php include('inc_footer.php'); ?>