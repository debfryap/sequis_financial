<?php $page = "our_product"; ?>

<?php include('inc_header.php'); ?>

<!-- middle -->

<section>

    <div class="wrapper">

        <div id="banner-content"><img src="images/slider/banner-product.jpg" alt="Our products"></div>

        <nav class="share">

            <div class="left">Share:

                <a href="#"><img src="images/material/nav-tw.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-fb.png" alt=""></a>

                <a href="#"><img src="images/material/nav-mail.png" alt=""></a>

            </div>

            <div class="right">

                <a href="#"><img src="images/material/nav-zoomin.png" alt=""></a>

                <a href="#" class="center"><img src="images/material/nav-zoomout.png" alt=""></a>

                <a href="#"><img src="images/material/nav-print.png" alt=""></a>

            </div>

        </nav>

        <aside>

            <ul class="submenu">

                <li><a href="product-01.php">Group Life</a></li>

                <li><a href="product-02.php" class="active">Group Health</a></li>

                <li><a href="product-03.php">Group Saving</a></li>
                
            </ul>

            <div class="side_link ">

                <div class="label">sequis link</div>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid1.png" alt="my sequis" /></span>

                    <span class="text">

                        <h6>my Sequis</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid2.png" alt="Sequisfriend"/></span>

                    <span class="text">

                        <h6>Sequisfriend</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

                <a href="">

                    <span class="ico"><img src="images/material/sq_link_mid3.png" alt="Calculator"/> </span>

                    <span class="text">

                        <h6>Calculator</h6>

                        <p>This is Photoshop's version  of Lorem Ipsum. </p>

                    </span>

                </a>

            </div>

            <address>

                <div class="label">get in touch</div>

                <div>

                    <img src="images/material/icon-pointer.png" alt="">

                    <h6>Sequis Group</h6>

                    <p>Sequis Center Lt. 5<br />

                        Jl. Jend. Sudirman No. 71<br />

                        Jakarta 12190, Indonesia<br />

                        T. +6221 5226 677<br />

                        F. +6221 5205 837

                    </p>

                </div>

                <a href="#">Get Direction</a> <a href="#">Send Message</a>

            </address>

        </aside>

        <div id="content">

            <nav class="breadcumb"><a href="#">Home</a> / <a href="#">Our Products </a> / <a href="#">Group Health </a></nav>

            <h2>Group Health</h2>

            <div class="img_content">

                <img src="images/content/img-about-02.jpg" alt="">

            </div>

            <h6>Group Health</h6>
            <p>Merupakan asuransi kesehatan kumpulan yang mencakup fasilitas rawat inap dan layanan pembedahan di rumah sakit dengan pelayanan 24 jam di seluruh dunia. Asuransi ini menyediakan fasilitas cashless dengan jaringan yang sangat luas dan merata di seluruh Indonesia. Cukup dengan menunjukkan kartu peserta, maka peserta langsung mendapat pelayanan kesehatan sesuai dengan kebutuhan menurut indikasi medis.</p>
<p>Manfaat yang bisa didapatkan antara lain biaya harian rumah sakit, biaya harian unit perawatan intensif, biaya pembedahan, kunjungan harian dokter rawat inap dan spesialis rawat inap, biaya lain-lain rawat inap, biaya ambulan, perawatan rawat jalan darurat kecelakaan, perawatan darurat kecelakaan gigi, perawat pribadi di rumah, biaya konsultasi medis, pemeriksaan laboratorium sebelum rawat inap, biaya perawatan setelah rawat inap serta pembedahan pulang hari.</p>
<ul class="ul_style">
  <li> <strong>Hospital Protection Plus (HPP)</strong><br />
    Paket asuransi kesehatan kumpulan dengan proses administrasi dan underwriting yang simple, <br />
    mudah dan cepat serta memiliki manfaat Plus yakni rawat inap dengan kartu di rumah sakit rekanan, <br />
  manfaat tahunan tidak terbatas (termasuk keluarga), serta pilihan tambahan manfaat penyakit kritis.<br /><br />Manfaat utama asuransi ini meliputi biaya rawat inap, biaya perawatan intensif, biaya konsultasi <br />
    dokter dan dokter spesialis, biaya perawat di rumah, biaya pembedahan dan one day care, biaya rawat <br />
  jalan dan gigi akibat kecelakaan, biaya perawatan lain-lain dan biaya sebelum dan sesudah rawat inap.</li>
  <li> <strong>Group Health Standard</strong><br />
    Paket asuransi kesehatan kumpulan yang ditujukan bagi karyawan korporasi. Manfaat utama <br />
    asuransi ini meliputi kecelakaan, sakit, cacat ataupun meninggal dunia. Terdiri dari beberapa produk <br />
    dan layanan asuransi yang secara fleksibel dapat disesuaikan untuk memenuhi kebutuhan rasa aman <br />
  bagi karyawan dan perusahaan.</li>
  <li> <strong>Maternity</strong><br />
    Asuransi tambahan (rider) yang memberi manfaat penggantian biaya melahirkan normal dan <br />
    pembedahan, ataupun keguguran secara medis. Selain itu terdapat manfaat pemeriksaan kehamilan <br />
  dan perawatan pasca proses kelahiran.</li>
  <li> <strong>Administrative Services Only (ASO)</strong><br />
    Merupakan layanan administrasi kepesertaan, administrasi klaim, pelaporan &amp; jasa lainnya yang dapat <br />
    dikombinasikan dengan mengambil salah satu produk asuransi Sequis Financial.<br />
  </li>
</ul>


        </div>

        <div class="clear"></div>

    </div>

</section>

<!-- end of middle -->

<?php include('inc_footer.php'); ?>